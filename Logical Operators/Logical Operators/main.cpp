#include <iostream>

using namespace std;

int main()
{
	/*AND   &&
	OR ||
	NOT !*/
	int x;
	cout << "Enter a value between 1 and 10\n";
	cin >> x;
	cin.ignore();
	cout << "Is the value beween 1 and 10?\n";
	cout << (1 <= x && x <= 10) << endl;

	cout << (!(2 + 3 == 7 && 5 > 3) || (7 * 3 <= 42)) << endl;
	system("pause");
	return 0;
}