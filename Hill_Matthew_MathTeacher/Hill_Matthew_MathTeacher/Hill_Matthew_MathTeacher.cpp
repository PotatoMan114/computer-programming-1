#include <iostream>
#include <math.h>

using namespace std;

int main()
{
	int shape;
	float radius;
	float pi;
	pi = 3.14159265359;
	float triangleBase, triangleHeight;
	float squareSideLength;
	cout << "This program will funciton as a caluculator for the area of a square, tirangle, or cirlce.\n";
	cout << "Please select what shape by typing in '1' for square, '2' for triangle, or '3' for circle.\n";
	cin >> shape;
	cin.ignore();
	if (shape == 1) {
		cout << "You selected square.\n";
		cout << "What is the side length of the square?\n";
		cin >> squareSideLength;
		cout << "The area of your square is ";
		cout << pow(squareSideLength, 2);
		cout << ".\n";
		cout << "Have a nice day!\n";
	}
	else if (shape == 2) {
		cout << "You selected triangle.\n";
		cout << "What is the base length of the triangle?\n";
		cin >> triangleBase;
		cout << "What is the height of the triangle?\n";
		cin >> triangleHeight;
		cout << "The area of the triangle is ";
		cout << triangleBase * triangleHeight / 2;
		cout << ".\n";
		cout << "Have a nice day!\n";
	}
	else if (shape == 3) {
		cout << "You selected circle.\n";
		cout << "What is the radius of the circle?\n";
		cin >> radius;
		cout << "The area of the circle is ";
		cout << pi * (pow(radius, 2));
		cout << ".\n";
		cout << "Have a nice day!\n";
	}
	else {
		cout << "You did not select a valid value. :(\n Please try again, but this time do it correctly!\n";
	}
	system("pause");
	return 0;
}