#include <iostream>
#include <string>
#include <vector>
using namespace std;

void main()
{
	//Declare a vector
	// vector<type> name;
	vector<string> message;

	//Add a value/element
	message.push_back("Cheese"); // Cheese
	message.push_back("Mouse"); // Cheese, Mouse

	//Insert a value
		//.begin points to first element, inserts immediately to the left of the pointed element
		//.end points to final element, inserts immediately to the right of the pointed element
		//Can add and subtract numbers after .begin or .end
	message.insert(message.begin(), "Purple"); // Purple, Cheese, Mouse
	message.insert(message.end(), "Shifty");  // Purple, Cheese, Mouse, Shifty
	message.insert(message.begin() + 2, "Tickles"); //Purple, Cheese, Tickles, Mouse, Shifty
	message.insert(message.end() - 1, "The"); //Purple, Cheese, Tickles, Mouse, The, Shifty

	//Display a vector
	for (int i = 0; i < message.size(); i++)
		cout << message[i] << " "; //Alternative: message.at(i)
	cout << endl;

	message.erase(message.begin() + 1); //Removes the selected element; removes "cheese"

	for (int i = 0; i < message.size(); i++)
		cout << message[i] << " ";
	cout << endl;

	message.clear(); //Completely clears the vector; returns it to how it was when declared.
		//When empty, vectorName.size() = 0
	for (int i = 0; i < message.size(); i++)
		cout << message[i] << " ";
	cout << endl;

	system("pause");

}