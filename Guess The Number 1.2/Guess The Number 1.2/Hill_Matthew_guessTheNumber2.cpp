#include <iostream>
#include <ctime>
using namespace std;

int main()
{
	int ansdigit1, ansdigit2, ansdigit3;
	int indigit1, indigit2, indigit3;
	bool matchdigit1, matchdigit2, matchdigit3;
	bool correct1, correct2, correct3, correct4, correct5;
	correct1 = false;
	correct2 = false;
	correct3 = false;
	correct4 = false;
	correct5 = false;
	srand(time(0));
	ansdigit1 = rand() % (10) + 1;
	ansdigit2 = rand() % (10) + 1;
	ansdigit3 = rand() % (10) + 1;

	//first
	{
		cout << "Welcome! Your job is to guess a series of three numbers in between 1 and 10 inclusive. Why? Becasue  Mr. Larsen said so.\n\n";
		cout << "What is your guess for the first number?\n";
		cin >> indigit1;
		cin.ignore();
		if (indigit1 == ansdigit1)
			matchdigit1 = true;
		else
			matchdigit1 = false;
		cout << "What is your guess for the second number?\n";
		cin >> indigit2;
		cin.ignore();
		if (indigit2 == ansdigit2)
			matchdigit2 = true;
		else
			matchdigit2 = false;
		cout << "What is your guess for the third number?\n";
		cin >> indigit3;
		cin.ignore();
		cout << endl;
		if (indigit3 == ansdigit3)
			matchdigit3 = true;
		else
			matchdigit3 = false;
		if (matchdigit1 && matchdigit2 && matchdigit3 == true)
		{
			cout << "Congrats! All three numbers were correct. How did you do that on the first try?\n";
			correct1 = true;
			correct2 = true;
			correct3 = true;
			correct4 = true;
			correct5 = true;
		}
		else
		{
			if (matchdigit1 == false)
			{
				cout << "The first number was incorrect.\n";
			}
			else
			{
				cout << "The first number was correct.\n";
			}
			if (matchdigit2 == false)
			{
				cout << "The second number was incorrect.\n";
			}
			else
			{
				cout << "The second number was correct.\n";
			}
			if (matchdigit3 == false)
			{
				cout << "The thrid number was incorrect.\n\n";
			}
			else
			{
				cout << "The third number was correct.\n\n";
			}
		}
	}
	
	//second
	if (correct1 == false)
	{
		cout << "This is your second attempt.\n";
		cout << "What is your guess for the first number?\n";
		cin >> indigit1;
		cin.ignore();
		if (indigit1 == ansdigit1)
			matchdigit1 = true;
		else
			matchdigit1 = false;
		cout << "What is your guess for the second number?\n";
		cin >> indigit2;
		cin.ignore();
		if (indigit2 == ansdigit2)
			matchdigit2 = true;
		else
			matchdigit2 = false;
		cout << "What is your guess for the third number?\n";
		cin >> indigit3;
		cin.ignore();
		cout << endl;
		if (indigit3 == ansdigit3)
			matchdigit3 = true;
		else
			matchdigit3 = false;
		if (matchdigit1 && matchdigit2 && matchdigit3 == true)
		{
			cout << "Congrats! All three numbers were correct.\n";
			correct2 = true;
			correct3 = true;
			correct4 = true;
			correct5 = true;
		}
		else
		{
			if (matchdigit1 == false)
			{
				cout << "The first number was incorrect.\n";
			}
			else
			{
				cout << "The first number was correct.\n";
			}
			if (matchdigit2 == false)
			{
				cout << "The second number was incorrect.\n";
			}
			else
			{
				cout << "The second number was correct.\n";
			}
			if (matchdigit3 == false)
			{
				cout << "The thrid number was incorrect.\n\n";
			}
			else
			{
				cout << "The third number was correct.\n\n";
			}
		}
	}

	//third
	if (correct2 == false)
	{
		cout << "This is your third attempt.\n";
		cout << "What is your guess for the first number?\n";
		cin >> indigit1;
		cin.ignore();
		if (indigit1 == ansdigit1)
			matchdigit1 = true;
		else
			matchdigit1 = false;
		cout << "What is your guess for the second number?\n";
		cin >> indigit2;
		cin.ignore();
		if (indigit2 == ansdigit2)
			matchdigit2 = true;
		else
			matchdigit2 = false;
		cout << "What is your guess for the third number?\n";
		cin >> indigit3;
		cin.ignore();
		cout << endl;
		if (indigit3 == ansdigit3)
			matchdigit3 = true;
		else
			matchdigit3 = false;
		if (matchdigit1 && matchdigit2 && matchdigit3 == true)
		{
			cout << "Congrats! All three numbers were correct.\n";
			correct3 = true;
			correct4 = true;
			correct5 = true;
		}
		else
		{
			if (matchdigit1 == false)
			{
				cout << "The first number was incorrect.\n";
			}
			else
			{
				cout << "The first number was correct.\n";
			}
			if (matchdigit2 == false)
			{
				cout << "The second number was incorrect.\n";
			}
			else
			{
				cout << "The second number was correct.\n";
			}
			if (matchdigit3 == false)
			{
				cout << "The thrid number was incorrect.\n\n";
			}
			else
			{
				cout << "The third number was correct.\n\n";
			}
		}
	}

	//fourth
	if (correct3 == false)
	{
		cout << "This is your fourth attempt.\n";
		cout << "What is your guess for the first number?\n";
		cin >> indigit1;
		cin.ignore();
		if (indigit1 == ansdigit1)
			matchdigit1 = true;
		else
			matchdigit1 = false;
		cout << "What is your guess for the second number?\n";
		cin >> indigit2;
		cin.ignore();
		if (indigit2 == ansdigit2)
			matchdigit2 = true;
		else
			matchdigit2 = false;
		cout << "What is your guess for the third number?\n";
		cin >> indigit3;
		cin.ignore();
		cout << endl;
		if (indigit3 == ansdigit3)
			matchdigit3 = true;
		else
			matchdigit3 = false;
		if (matchdigit1 && matchdigit2 && matchdigit3 == true)
		{
			cout << "Congrats! All three numbers were correct.\n";
			correct4 = true;
			correct5 = true;
		}
		else
		{
			if (matchdigit1 == false)
			{
				cout << "The first number was incorrect.\n";
			}
			else
			{
				cout << "The first number was correct.\n";
			}
			if (matchdigit2 == false)
			{
				cout << "The second number was incorrect.\n";
			}
			else
			{
				cout << "The second number was correct.\n";
			}
			if (matchdigit3 == false)
			{
				cout << "The thrid number was incorrect.\n\n";
			}
			else
			{
				cout << "The third number was correct.\n\n";
			}
		}
	}

	//fifth
	if (correct4 == false)
	{
		cout << "This is your final attempt. Don't mess it up!\n";
		cout << "What is your final guess for the first number?\n";
		cin >> indigit1;
		cin.ignore();
		if (indigit1 == ansdigit1)
			matchdigit1 = true;
		else
			matchdigit1 = false;
		cout << "What is your final guess for the second number?\n";
		cin >> indigit2;
		cin.ignore();
		if (indigit2 == ansdigit2)
			matchdigit2 = true;
		else
			matchdigit2 = false;
		cout << "What is your final guess for the third number?\n";
		cin >> indigit3;
		cout << endl;
		cin.ignore();
		if (indigit3 == ansdigit3)
			matchdigit3 = true;
		else
			matchdigit3 = false;
		if (matchdigit1 && matchdigit2 && matchdigit3 == true)
		{
			cout << "Congrats! All three numbers were correct. You got it right at the end!\n";
			correct5 = true;
		}
		else
		{
			if (matchdigit1 == false)
			{
				cout << "The first number was incorrect.\nThe correct answer was " << ansdigit1 << ".\n";
			}
			else
			{
				cout << "The first number was correct.\n";
			}
			if (matchdigit2 == false)
			{
				cout << "The second number was incorrect.\nThe correct answer was " << ansdigit2 << ".\n";
			}
			else
			{
				cout << "The second number was correct.\n";
			}
			if (matchdigit3 == false)
			{
				cout << "The thrid number was incorrect.\nThe correct answer was " << ansdigit3 << ".\n\n";
			}
			else
			{
				cout << "The third number was correct.\n\n";
			}
			cout << "That was your last try. Better luck next time.\n";
		}
	}
	system("pause");
	return 0;
}