#include <iostream>
#include <ctime>

using namespace std;

int main()
{
	int firstNum, secondNum;
	bool result;
	cout << "Enter the first number:\n";
	cin >> firstNum;
	cin.ignore();
	cout << "Enter the second number:\n";
	cin >> secondNum;
	cin.ignore();

	cout << "Is the first number less than the second?\n";
	result = firstNum < secondNum;
	cout << result << endl;
	cout << "Is the first number less than or equal to the second?\n";
	cout << (firstNum <= secondNum) << endl;
	cout << "Is the first number greater than the second?\n";
	cout << (firstNum > secondNum) << endl;
	cout << "Is the first number greater than or equal to the second?\n";
	cout << (firstNum >= secondNum) << endl;
	cout << "Is the first number equal to the second?\n";
	cout << (firstNum == secondNum) << endl;
	cout << "Is the first number not equal to the second?\n";
	cout << (firstNum != secondNum) << endl;

	//Random Number Generator
	srand(time(0)); //seeds the random algorithm
	//rand() % (highest - lowest) + lowest
	cout << rand() % (10) + 1 << endl;
	cout << rand() % (10) + 1 << endl;
	cout << rand() % (10) + 1 << endl;
	cout << rand() % (10) + 1 << endl;
	cout << rand() % (10) + 1 << endl;

	system("pause");
	return 0;
}