#include <iostream>
#include <ctime>

using namespace std;

//Custom Functions declared
int random();
int multiply(int, int);
void end(int, int, int);


//Main
int main()
{
	srand(time(0));
	cout << "Hi! This program really doesn't serve a purpose.\nThe program will randomly select a number between 2 and 10\n";
	int rand1 = random();
	cout << "That number is " << rand1 << ".\n";
	cout << "Now we're going to get another random number to mulitply the original by.\n";
	int rand2 = random();
	int ans1 = multiply(rand1, rand2);
	cout << "The numbers we are mulitplying are " << rand1 << " and " << rand2 << ".\nThe result is " << ans1 << ".\n\n";
	cout << "Now, with those numbers, we're gonna do something magical.\n";
	end(rand1, rand2, ans1);
	cout << "Have and nice day! Maybe do something a little more productive than run a completey useless program.\n";
	system("pause");
	return 0;
}

//Custom Functions defined

int random()
{
	return rand() % (10) + 2;
}

int multiply(int x, int y)
{
	return x*y;
}

void end(int rand1, int rand2, int ans1)
{
	int ans2 = ans1 - rand1 - rand2;
	cout << ans2 << endl;
	cout << "That's it!\n";
}