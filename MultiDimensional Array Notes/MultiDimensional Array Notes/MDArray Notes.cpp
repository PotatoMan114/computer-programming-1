#include <iostream>
using namespace std;

void main()
{
	int num1[8]; //1D
	double num2[6][8]; //2D
	char char1[5][5][3]; //3D

	char1[2][3][0] = 'B';

	for (int x = 0; x < 6; x++)
	{
		for (int y = 0; y < 8; y++)
		{
			num2[x][y] = y;
		}
	}

	for (int x = 0; x < 6; x++)
	{
		for (int y = 0; y < 8; y++)
		{
			cout << num2[x][y];
		}
		cout << endl;
	}
	system("pause");
}
//x = column, y = row, z = depth