#include <iostream>
#include <ctime>
#include <string>
using namespace std;

//Global variables
string MESSAGE;

//All custon funcitons must be declared here
//However, you don't have to define them here unless you want to
void cheese() //Declaring and defining before main()
{
	cout << MESSAGE << endl;
}

int purple(int); //Declared but not defined. It will be defined after main()

int main()
{
	int y;
	srand(time(0));
	cout << "Enter in a message\n";
	getline(cin, MESSAGE);
	cout << "Enter in any positive number.\n";
	cin >> y;
	for (int i = 0; i < purple(y); i++)
		cheese();
	system("pause");
	return 0;
}

//If you declared a function, but didn't define it, then you will define it here.
//Paramaters MUST be in the same order when it is called and when it is declared.
int purple(int x)
{
	return (rand() % x + 1);
}