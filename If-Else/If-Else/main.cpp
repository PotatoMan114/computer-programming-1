/*If-Else commands folow the English language
if (condition)
{
*code for when condition is met
}
else //Not always required
{
*code for when condition is not met
}
*/
#include <iostream>

using namespace std;

int main()
{
	int choice;
	cout << "Enter in a value between 1 - 5 for a special message:\n";
	cin >> choice;
	cin.ignore();
	if (choice == 1)
	{
		cout << "My people skills are rusty.\n";
	}
	else if (choice == 2)
	{
		cout << "It is not of import.\n";
	}
	else if (choice == 3)
	{
		cout << "I think we can rule Moses out as a suspect.\n";
	}
	else if (choice == 4)
	{
		cout << "This isn't funny Dean! The voice says I'm almost out of minutes!\n";
	}
	else if (choice == 5)
	{
		cout << "I don't sleep.\n";
	}
	else
	{
		cout << "Sorry, you didn't enter a correct value. No message for you.\n";
		if (choice == 420)
		{
			cout << "Blaze it!\n";
		}
	}
	system("pause");
	return 0;
}