#include <iostream>
#include <ctime>

using namespace std;

int main()
{
	int answer;
	int in1, in2, in3, in4, in5;
	srand(time(0));
	answer = rand() % (10) + 1;
	cout << "Hi! This program has already determined a numebr between 1 and 10 inclusive.\nYour job is to guess what number it is.\nDon't ask why, I don't make the rules.\n";
	cout << "So, what is your first guess?\n";
	cin >> in1;
	if (in1 == answer)
	{
		cout << "Well, that was easy. Yes, the answer was " << answer << "!\n Good job!\n";
	}
	else
	{
		cout << "Nope!\n";
		if (in1 < answer)
		{
			cout << "The correct answer is greater than " << in1 << endl;
		}
		else
		{
			cout << "The correct answer is less than " << in1 << endl;
		}
		
		cout << "So, what is your next guess?\n";
		cin >> in2;
		if (in2 == answer)
		{
			cout << "Nice job! That was the correct answer!";
		}
		else
		{
			cout << "Nope!\n";
			if (in2 < answer)
			{
				cout << "The correct answer is greater than " << in2 << endl;
			}
			else
			{
				cout << "The correct answer is less than " << in2 << endl;
			}
			
			cout << "So, what is your next guess?\n";
			cin >> in3;
			if (in3 == answer)
			{
				cout << "Nice job! That was the correct answer!";
			}
			else
			{
				cout << "Nope!\n";
				if (in3 < answer)
				{
					cout << "The correct answer is greater than " << in3 << endl;
				}
				else
				{
					cout << "The correct answer is less than " << in3 << endl;
				}

				cout << "So, what is your next guess?\n";
				cin >> in4;
				if (in4 == answer)
				{
					cout << "Nice job! That was the correct answer!";
				}
				else
				{
					cout << "Nope!\n";
					if (in4 < answer)
					{
						cout << "The correct answer is greater than " << in4 << endl;
					}
					else
					{
						cout << "The correct answer is less than " << in4 << endl;
					}
					cout << "Last attempt! You can do this!\n";
					cin >> in5;
					if (in5 == answer)
					{
						cout << "Awesome! That was the correct answer!\n";
					}
					else
					{
						cout << "Not quite. The correct answer is " << answer << ". Maybe next time.\n";
					}
				}
			}
		}
	}
	system("pause");
	return 0;
}