#include <iostream>
#include <string>

using namespace std;
/*Notes
	Figure out how to do a hard return within the output.
	
	*/
int main()
{
	string Ans1, Ans2, Ans3, Ans4, Ans5; //One variable for each answer.
	cout << "Hello! This is a quick questionare.\n";
	cout << "You will be asked to answer five questions, and then your responces will be repeated back to you in reverse order at the end.\n";
	cout << "Let's get started.\n";
	cout << "Question 1: Do you own a pet?\n";
	cin >> Ans1; //User input gives Ans1 variable a value
	cout << "Question 2: What is your favorite color?\n";
	cin >> Ans2;
	cout << "Question 3: What is your favorite video game?\n";
	cin >> Ans3;
	cout << "Question 4: Do/Did you like school?\n";
	cin >> Ans4;
	cout << "Question 5: Do you like cheese?\n";
	cin >> Ans5;
	//End of questions, now it repeates the answers back in reverse order
	cout << "That's all the questions, here is what you answered:\n";
	cout << "For Question 5 you answered " << Ans5 << endl;
	cout << "For Question 4 you answered " << Ans4 << endl;
	cout << "For Question 3 you answered " << Ans3 << endl;
	cout << "For Question 2 you answered " << Ans2 << endl;
	cout << "For Question 1 you answered " << Ans1 << endl;
	cout << "That's it! Have a nice day!";
	cin.get();
	cin.get();
	return 0;
}