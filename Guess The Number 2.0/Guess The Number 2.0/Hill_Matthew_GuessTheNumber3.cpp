#include <iostream>
#include <ctime>
using namespace std;

int main()
{
	srand(time(0));
	int answer = rand() % (100) + 1;
	int guess;
	int attempt = 1;

	cout << "Why, hello there!\nI'm thinking of a number, and you've got to guess it. Why? Because Mr. Larsen said so!\nYou guess an interger between 1 and 100 inclusive and you get told if you are right or wrong.\nLet's have some fun!\n\n";
	cout << "What is your first guess?\n";
	cin >> guess;
	cin.ignore();

	if (guess == answer)
	{
		cout << "Wha-- how... Well, nice job. You got it on the first try. Awesome!";
	}
	else
	{
		while (guess != answer)
		{
			attempt++;
			cout << "That was the incorrect answer.\n";
			if (guess < answer)
				cout << "The answer is greater than you think.\n";
			else
				cout << "The answer is less than you think.\n";
			cout << "What is your next guess?\n";
			cin >> guess;
			cin.ignore();
		}
		cout << "Congrats, you got the correct answer! It only took you " << attempt << " attempts!\n";
	}
	system("pause");
	return 0;
}