#include <iostream> //Accessing the iostream (input-output stream) library

using namespace std;

int main() //This is where the program begins.
{
	cout << "GOOD MORNING VIETNAM!!!"; //outputs to screen
	cin.get(); //Pauses as it is waiting for input.
	return 0; //This will exit the program.
}