#include <iostream>
#include <ctime>
#include <math.h>
#include <ctype.h>
using namespace std;

int main()
{
	//computes area of a circle using the pow function from <math.h>
	cout << "The beginning part of this program will compute the area of a circle to .\nWhat is the radius of the circle?\n";
	float radius;
	cin >> radius;
	cout << "The area of the circle is " << (pow(radius, 2.0)*3.1415926) << ".\n\n";
	system("pause");
	system("cls");

	//This part will tell you the square root of an interger using the sprt function from <math.h>
	cout << "This part will compute the square root of any whole number value.\nWhat is the number you would like to input?\n";
	int squareRootIn;
	cin >> squareRootIn;
	if (squareRootIn < 0)
	{
		cout << "I said whole number, ya dummy!\nWell, it sucks to be you. Next function!\n\n";
	}
	else
	{
		cout << "The square root of " << squareRootIn << " is " << sqrt(squareRootIn) << ".\n\n";
	}
	system("pause");
	system("cls");

	//Calculates the absolute value of the cubic root of a number using the cbrt and fabs functions from the <math.h> library.
	cout << "Okay, now I'm going to calculate the absolute value of the cube root of a number.\nHave at it. What's the number?\n";
	float cbrtin;
	cin >> cbrtin;
	cout << abs(cbrt(cbrtin)) << endl;
	system("pause");
	system("cls");

	//Tests if a charcter input is blank using the isupper function from the <ctype.h> library, then outputs it as a lower case
	cout << "Here, you don't have a choice.\nI'm going to change all the upper case characters in the sentence 'Test String.' to lower case.\n";
	int i = 0;
	char str[] = "Test String.\n";
	char c;
	while (str[i])
	{
		c = str[i];
		if (isupper(c)) c = tolower(c);
		putchar(c);
		i++;
	}
	system("pause");
	system("cls");
	return 0;
}