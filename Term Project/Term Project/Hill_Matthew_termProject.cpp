//Beta Test
// Matthew Hill 1B
#include <iostream>
#include <ctime>
#include <math.h>
#include <string>

using namespace std;

int main()
{
	//Variables
		char A1, A2, A3, B1, B2, B3, C1, C2, C3;
		A1 = ' ';
		A2 = ' ';
		A3 = ' ';
		B1 = ' ';
		B2 = ' ';
		B3 = ' ';
		C1 = ' ';
		C2 = ' ';
		C3 = ' ';
		string input;
		string player1, player2;
		bool valid = false;
		bool complete = false;
		bool p1win = false;
		bool p2win = false;

	//Introduction
		cout << "Hello and welcome! You have singed up your soul to a game of tic-tac-toe. You only have yourself to blame.\n";
		cout << "So, what is your name, player 1.\n";
		cin >> player1;
		cin.ignore();
		cout << "Congrats, " << player1 << ", you will be the X!\n";
		cout << "What about you, player 2? What is your name?\n";
		cin >> player2;
		cin.ignore();
		cout << "Well, " << player2 << ", you're going to be O. Good luck.\n\n";

		cout << "So, now for the game!!!!!\n";
		cout << "Here is what the board looks like:\n\n";

		cout << "     1   2   3 " << endl;
		cout << "  A  " << A1 << " | " << A2 << " | " << A3 << " " << endl;
		cout << "    ---+---+---" << endl;
		cout << "  B  " << B1 << " | " << B2 << " | " << B3 << " " << endl;
		cout << "    ---+---+---" << endl;
		cout << "  C  " << C1 << " | " << C2 << " | " << C3 << " " << endl;
		cout << "\n";
		system("pause");
		cout << "\n";

		//Loop
		while (complete != true)
		{
			do
			{
				//P1 input
				//Asks P1 for location
				cout << "Choose the location of your X, " << player1 << " .\n";
				cout << "Please put the row letter first and the column number second with no space (i.e. A3).\n";
				cin >> input;
				cin.ignore();

				//Checks the result
				if ((input == "A1") || (input == "a1"))
				{
					if (A1 != 'X' && A1 != 'O')
					{
						A1 = 'X';
						valid = true;
					}
					else
					{
						cout << "That space is already taken.\n";
						valid = false;
					}
				}
				else if ((input == "A2") || (input == "a2"))
				{
					if (A2 != 'X' && A2 != 'O')
					{
						A2 = 'X';
						valid = true;
					}
					else
					{
						cout << "That space is already taken.\n";
						valid = false;
					}
				}
				else if ((input == "A3") || (input == "a3"))
				{
					if (A3 != 'X' && A3 != 'O')
					{
						A3 = 'X';
						valid = true;
					}
					else
					{
						cout << "That space is already taken.\n";
						valid = false;
					}
				}
				else if ((input == "B1") || (input == "b1"))
				{
					if (B1 != 'X' && B1 != 'O')
					{
						B1 = 'X';
						valid = true;
					}
					else
					{
						cout << "That space is already taken.\n";
						valid = false;
					}
				}
				else if ((input == "B2") || (input == "b2"))
				{
					if (B2 != 'X' && B2 != 'O')
					{
						B2 = 'X';
						valid = true;
					}
					else
					{
						cout << "That space is already taken.\n";
						valid = false;
					}
				}
				else if ((input == "B3") || (input == "b3"))
				{
					if (B3 != 'X' && B3 != 'O')
					{
						B3 = 'X';
						valid = true;
					}
					else
					{
						cout << "That space is already taken.\n";
						valid = false;
					}
				}
				else if ((input == "C1") || (input == "c1"))
				{
					if (C1 != 'X' && C1 != 'O')
					{
						C1 = 'X';
						valid = true;
					}
					else
					{
						cout << "That space is already taken.\n";
						valid = false;
					}
				}
				else if ((input == "C2") || (input == "c2"))
				{
					if (C2 != 'X' && C2 != 'O')
					{
						C2 = 'X';
						valid = true;
					}
					else
					{
						cout << "That space is already taken.\n";
						valid = false;
					}
				}
				else if ((input == "C3") || (input == "c3"))
				{
					if (C3 != 'X' && C3 != 'O')
					{
						C3 = 'X';
						valid = true;
					}
					else
					{
						cout << "That space is already taken.\n";
						valid = false;
					}
				}
				else
				{
					cout << "Please enter a valid input.";
					valid = false;
				}
			} while (valid == false);
			valid = false;

			//Shows board so far
			cout << "Okay, this is the board so far.\n\n";
			cout << "     1   2   3 " << endl;
			cout << "  A  " << A1 << " | " << A2 << " | " << A3 << " " << endl;
			cout << "    ---+---+---" << endl;
			cout << "  B  " << B1 << " | " << B2 << " | " << B3 << " " << endl;
			cout << "    ---+---+---" << endl;
			cout << "  C  " << C1 << " | " << C2 << " | " << C3 << " " << endl;
			cout << "\n";

			//Checks to see if game is complete
			if (((A1 == 'X') && (A2 == 'X') && (A3 == 'X')) || ((B1 == 'X') && (B2 == 'X') && (B3 == 'X')) || ((C1 == 'X') && (C2 == 'X') && (C3 == 'X')) || ((A1 == 'X') && (B1 == 'X') && (C1 == 'X')) || ((A2 == 'X') && (B2 == 'X') && (C2 == 'X')) || ((A3 == 'X') && (B3 == 'X') && (C3 == 'X')) || ((A1 == 'X') && (B2 == 'X') && (C3 == 'X')) || ((A3 == 'X') && (B2 == 'X') && (C1 == 'X')))
			{
				complete = true;
				p1win = true;
				break;
			}
			else if ((A1 != ' ') && (A2 != ' ') && (A3 != ' ') && (B1 != ' ') && (B2 != ' ') && (B3 != ' ') && (C1 != ' ') && (C2 != ' ') && (C3 != ' '))
			{
				complete = true;
				break;
			}
			system("pause");
			cout << "\n";

			do
			{
				//P2 second input
				//Asks P2 for location
				cout << "Okay, " << player2 << ", where would you like to place your O?\n";
				cout << "Please put the row letter first and the column number second with no space (i.e. A3).\n";
				cin >> input;
				cin.ignore();

				//Checks the result
				if ((input == "A1") || (input == "a1"))
				{
					if (A1 != 'X' && A1 != 'O')
					{
						A1 = 'O';
						valid = true;
					}
					else
					{
						cout << "That space is already taken.\n";
						valid = false;
					}
				}
				else if ((input == "A2") || (input == "a2"))
				{
					if (A2 != 'X' && A2 != 'O')
					{
						A2 = 'O';
						valid = true;
					}
					else
					{
						cout << "That space is already taken.\n";
						valid = false;
					}
				}
				else if ((input == "A3") || (input == "a3"))
				{
					if (A3 != 'X' && A3 != 'O')
					{
						A3 = 'O';
						valid = true;
					}
					else
					{
						cout << "That space is already taken.\n";
						valid = false;
					}
				}
				else if ((input == "B1") || (input == "b1"))
				{
					if (B1 != 'X' && B1 != 'O')
					{
						B1 = 'O';
						valid = true;
					}
					else
					{
						cout << "That space is already taken.\n";
						valid = false;
					}
				}
				else if ((input == "B2") || (input == "b2"))
				{
					if (B2 != 'X' && B2 != 'O')
					{
						B2 = 'O';
						valid = true;
					}
					else
					{
						cout << "That space is already taken.\n";
						valid = false;
					}
				}
				else if ((input == "B3") || (input == "b3"))
				{
					if (B3 != 'X' && B3 != 'O')
					{
						B3 = 'O';
						valid = true;
					}
					else
					{
						cout << "That space is already taken.\n";
						valid = false;
					}
				}
				else if ((input == "C1") || (input == "c1"))
				{
					if (C1 != 'X' && C1 != 'O')
					{
						C1 = 'O';
						valid = true;
					}
					else
					{
						cout << "That space is already taken.\n";
						valid = false;
					}
				}
				else if ((input == "C2") || (input == "c2"))
				{
					if (C2 != 'X' && C2 != 'O')
					{
						C2 = 'O';
						valid = true;
					}
					else
					{
						cout << "That space is already taken.\n";
						valid = false;
					}
				}
				else if ((input == "C3") || (input == "c3"))
				{
					if (C3 != 'X' && C3 != 'O')
					{
						C3 = 'O';
						valid = true;
					}
					else
					{
						cout << "That space is already taken.\n";
						valid = false;
					}
				}
				else
				{
					cout << "Please enter a valid input.\n";
					valid = false;
				}
			} while (valid == false);
			valid = false;

			//Shows the board so far
			cout << "Okay, this is the board so far.\n\n";
			cout << "     1   2   3 " << endl;
			cout << "  A  " << A1 << " | " << A2 << " | " << A3 << " " << endl;
			cout << "    ---+---+---" << endl;
			cout << "  B  " << B1 << " | " << B2 << " | " << B3 << " " << endl;
			cout << "    ---+---+---" << endl;
			cout << "  C  " << C1 << " | " << C2 << " | " << C3 << " " << endl;
			cout << "\n";
			
			if (((A1 == 'O') && (A2 == 'O') && (A3 == 'O')) || ((B1 == 'O') && (B2 == 'O') && (B3 == 'O')) || ((C1 == 'O') && (C2 == 'O') && (C3 == 'O')) || ((A1 == 'O') && (B1 == 'O') && (C1 == 'O')) || ((A2 == 'O') && (B2 == 'O') && (C2 == 'O')) || ((A3 == 'O') && (B3 == 'O') && (C3 == 'O')) || ((A1 == 'O') && (B2 == 'O') && (C3 == 'O')) || ((A3 == 'O') && (B2 == 'O') && (C1 == 'O')))
			{
				complete = true;
				p2win = true;
				break;
			}
			else if ((A1 == 'O') && (A1 == 'X') && (A2 == 'O') && (A2 == 'X') && (A3 == 'O') && (A3 == 'X') && (B1 == 'O') && (B1 == 'X') && (B2 == 'O') && (B2 == 'X') && (B3 == 'O') && (B3 == 'X') && (C1 == 'O') && (C1 == 'X') && (C2 == 'O') && (C2 == 'X') && (C3 == 'O') && (C3 == 'X'))
			{
				complete = true;
				break;
			}
			system("pause");
			cout << "\n";
		}

		//End
		if (p1win == true)
		{
			cout << "Congratulations! " << player1 << " is the winner! You get to keep your soul.\n";
		}
		else if (p2win == true)
		{
			cout << "Congratulations! " << player2 << " is the winner! You get to keep your soul.\n";
		}
		else
		{
			cout << "Welp... no one won. Both your souls are mine.";
		}
	system("pause");
	return 0;
}
/*

cout << "     1   2   3 " << endl;
cout << "  A  " << A1 << " | " << A2 << " | " << A3 << " " << endl;
cout << "    ---+---+---" << endl;
cout << "  B  " << B1 << " | " << B2 << " | " << B3 << " " << endl;
cout << "    ---+---+---" << endl;
cout << "  C  " << C1 << " | " << C2 << " | " << C3 << " " << endl;
cout << "\n";
system("pause");
cout << "\n";

*/
/*
   1   2   3 
A    |   |   
  ---+---+---
B    |   |   
  ---+---+---
C    |   |   
*/