#include <iostream>
#include <string>
using namespace std;

int main()
{
	//Declare and define
	string roman[] = { "I", "II", "III", "IV", "V", "VI", "VII", "VIII", "IX", "X", "XI", "XII", "XIII", "XIV", "XV", "XVI", "XVII", "XVIII", "XIX", "XX", "XXI", "XXII", "XXIII", "XXIV", "XXV", "XXVI", "XXVII", "XXVIII", "XXIX", "XXX", "XXXI", "XXXII", "XXXIII", "XXXIV", "XXXV", "XXXVI", "XXXVII", "XXXVIII", "XXXIX", "XL", "XLI", "XLII", "XLIII", "XLIV", "XLV", "XLVI", "XLVII", "XLVIII", "XLIX", "L", "LI", "LII", "LIII", "LIV", "LV", "LVI", "LVII", "LVIII", "LIX", "LX", "LXI", "LXII", "LXIII", "LXIV", "LXV", "LXVI", "LXVII", "LXVIII", "LXIX", "LXX", "LXXI", "LXXII", "LXXIII", "LXXIV", "LXXV", "LXXVI", "LXXVII", "LXXVIII", "LXXIX", "LXXX", "LXXXI", "LXXXII", "LXXXIII", "LXXXIV", "LXXXV", "LXXXVI", "LXXXVII", "LXXXVIII", "LXXXIX", "XC", "XCI", "XCII", "XCIII", "XCIV", "XCV", "XCVI", "XCVII", "XCVIII", "XCIX", "C" };
	int number;
	cout << "This program will convert any number from one to one hundred inclusive to roman numerals." << endl;
	for (;;)
	{
		cout << "What number would you like to convert? (Enter 0 to exit the program)" << endl;
		cin >> number;
		cin.ignore();
		if (number == 0)
			break;
		else if (number < 0)
			cout << "Please enter a valid number between 0 and 100 inclusive.\n";
		else if (number > 100)
			cout << "Please enter a valid number between 0 and 100 inclusive.\n";
		else
		{
			cout << roman[(number - 1)] << endl;
			system("pause");
		}
	}
	cout << "Have a nice day." << endl;
	system("pause");
	return 0;
}
