#include <iostream>
#include <iomanip>
using namespace std;

char board[6][6];

void displayBoard()
{
	cout << "Here is the current board:\n";
	cout << setw(3);
	for (int i = 1; i <= 6; i++)
	{
		cout << " " << i;
	}
	cout << endl;
	for (int x = 0; x < 6; x++)
	{
		cout << x + 1 << " |";
		for (int y = 0; y < 6; y++)
		{
			cout << setw(1) << board[x][y] << "|";
		}
		cout << "\n";
	}
}

int main()
{
	for (;;)
	{
		char player[4];
		int times = 0;
		char winner = ' ';
		int playAgain;
		bool someoneWon = false;
		player[0] = 'X', player[1] = 'O', player[2] = 'T', player[3] = 'A';
		int rowIn;
		int colIn;
		bool gameComplete = false;
		int playerTurn = 0;
		//Initializes the board
		for (int x = 0; x < 6; x++)
		{
			for (int y = 0; y < 6; y++)
			{
				board[x][y] = ' ';
			}
		}

		//Introduction
		cout << "Welcome to Tic-Tac-Toe!\nThere are four players on a 6x6 grid, still 3 in a row to win.\nHave fun!\n";
		cout << "Player 1 is X.\nPlayer 2 is O.\nPlayer 3 is T.\nPlayer 4 is A.\n";
		system("pause");
		do
		{
			for (;;)
			{
				system("cls");
				displayBoard();
				cout << "Enter the row that you would like to put your letter (1-6).\n";
				cin >> colIn;
				cout << "Enter the column that you would like to put your letter (1-6).\n";
				cin >> rowIn;

				if (board[colIn-1][rowIn-1] == ' ')
					break;
				else
				{
					cout << "There is already something there. Try again.";
					system("pause");
				}

			}
			board[colIn-1][rowIn-1] = player[playerTurn];
			times++;
			//Test to see if someone won.
			for (int x = 0; x < 6; x++)
			{
				for (int y = 0; y < 6; y++)
				{
					if (board[x][y] != ' ')
					{
						if (x < 4)
						{
							if ((board[x + 1][y] == board[x][y]) && (board[x + 2][y] == board[x][y]))
							{
								gameComplete = true;
								winner = board[x][y];
								break;
							}
						}
						if (y < 4)
						{
							if ((board[x][y + 1] == board[x][y]) && (board[x][y + 2] == board[x][y]))
							{
								gameComplete = true;
								winner = board[x][y];
								break;
							}
						}
						if ((x > 1) && (y > 1))
						{
							if ((board[x - 1][y - 1] == board[x][y]) && (board[x - 2][y - 2] == board[x][y]))
							{
								gameComplete = true;
								winner = board[x][y];
								break;
							}
						}
						if ((x < 4) && (y < 4))
						{
							if((board[x + 1][y + 1] == board[x][y]) && (board[x + 2][y + 2] == board[x][y]))
							{
								gameComplete = true;
								winner = board[x][y];
								break;
							}
						}
					}
				}
				if (gameComplete == true)
					break;
			}
			if (times == 36)
			{
				gameComplete = true;
				winner = 'N';
			}
			if (playerTurn != 3)
				playerTurn++;
			else
				playerTurn = 0;
		} while (gameComplete == false);
		system("cls");
		displayBoard();
		//Endgame
		if (winner == 'N')
			cout << "No one wins. :(\n";
		else
			cout << winner << " is the winner! Good job!\n\n";
		cout << "Would you like to play again? (0: No, 1: Yes)\n";
		cin >> playAgain;
		system("cls");
		if (playAgain == 0)
			break;
	}
	return 0;
}