#include <iostream> //Accessing the iostream (input-output stream) library
#include <string> //Incudes the string library

using namespace std;

int main() //This is where the program begins.
{
	cout << "GOOD MORNING VIETNAM!!!"; //outputs to screen.
	int x, y, z;
	cout << "Enter a value for x: ";
	cin >> x; //User input = x
	cout << "Enter a value for y: ";
	cin >> y; //User input = y
	z = x / y;
	cout << "The value in z is: " << z << endl;
	cin.get();
	cin.get();
	return 0; //This will exit the program.
}