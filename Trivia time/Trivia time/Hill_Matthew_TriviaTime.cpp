#include <iostream>
#include <ctime>
#include <string>

using namespace std;
int main()
{
	string set1q1, set1q2, set1q3, set1q4, set1q5, set2q1, set2q2, set2q3, set2q4, set2q5;
	string set1ans1, set1ans2, set1ans3, set1ans4, set1ans5, set2ans1, set2ans2, set2ans3pos1, set2ans3pos2, set2ans4, set2ans5pos1, set2ans5pos2;
	string set1in, set2in;
	int ret1, ret2;
	bool correct1, correct2;
	int choice1, choice2;
	
	//set 1
	{
		set1q1 = "In what year was Super Mario Bros for the NES released in North America? (yyyy)";
		set1ans1 = "1985";
		set1q2 = "In what year was Super Mario 64 released in North America? (yyyy)";
		set1ans2 = "1996";
		set1q3 = "In what year was the Nintendo 64 released in North America? (yyyy)";
		set1ans3 = "1996";
		set1q4 = "In what year was The Legend of Zelda for the NES released in North America? (yyyy)";
		set1ans4 = "1986";
		set1q5 = "In what year was Metroid for the NES released in North America? (yyyy)";
		set1ans5 = "1986";
	}
	//set 2
	{
		set2q1 = ""
		set2ans1 = 
		set2q2 = 
		set2ans2 = 
		set2q3 = 
		set2ans3pos1 = 
		set2ans3pos2 = 
		set2q4 = 
		set2ans4 = 
		set2q5 = 
		set2ans5pos1 = 
		set2ans5pos2 = 
	}
	
	//RNG
	{
		srand(time(0));
		choice1 = rand() % (5) + 1;
		choice2 = rand() % (5) + 1;
		choice1 = 1;
		choice2 = 1;
	}
	
	//Start
	{
		cout << "Hello! Welcome to a random trivia game!\n";
		cout << "You will be asked two questions which you will have to answer.\n";
		cout << "You will be told if you got the question correct after you answer it.\n";
		cout << "Be sure to format your answers as shown in each question.\n";
		cout << "Let's begin!\n\n";
	}

	//Question 1
	{
		switch (choice1)
		{
		case 1:
		{
			cout << set1q1 << endl;
			cin >> set1in;
			cin.ignore();
			if (set1in == set1ans1)
			{
				cout << "Congrats! That was the correct answer. Now for the next question.\n\n";
				correct1 = true;
			}
			else
			{
				cout << "Nope! That was not the correct answer. Maybe you'll do better on the next question.\n\n";
				correct1 = false;
			}
			break;
		}
		case 2:
		{
			cout << set1q2 << endl;
			cin >> set1in;
			cin.ignore();
			if (set1in == set1ans2)
			{
				cout << "Congrats! That was the correct answer. Now for the next question.\n\n";
				correct1 = true;
			}
			else
			{
				cout << "Nope! That was not the correct answer. Maybe you'll do better on the next question.\n\n";
				correct1 = false;
			}
			break;
		}
		case 3:
		{
			cout << set1q3 << endl;
			cin >> set1in;
			cin.ignore();
			if (set1in == set1ans3)
			{
				cout << "Congrats! That was the correct answer. Now for the next question.\n\n";
				correct1 = true;
			}
			else
			{
				cout << "Nope! That was not the correct answer. Maybe you'll do better on the next question.\n\n";
				correct1 = false;
			}
			break;
		}
		case 4:
		{
			cout << set1q4 << endl;
			cin >> set1in;
			cin.ignore();
			if (set1in == set1ans4)
			{
				cout << "Congrats! That was the correct answer. Now for the next question.\n\n";
				correct1 = true;
			}
			else
			{
				cout << "Nope! That was not the correct answer. Maybe you'll do better on the next question.\n\n";
				correct1 = false;
			}
			break;
		}
		case 5:
		{
			cout << set1q5 << endl;
			cin >> set1in;
			cin.ignore();
			if (set1in == set1ans5)
			{
				cout << "Congrats! That was the correct answer. Now for the next question.\n\n";
				correct1 = true;
			}
			else
			{
				cout << "Nope! That was not the correct answer. Maybe you'll do better on the next question.\n\n";
				correct1 = false;
			}
			break;
		}
		default:
			break;
		}
	}

	//Question 2
	{
		switch (choice2)
		{
		case 1:
		{
			cout << set2q1 << endl;
			cin >> set2in;
			cin.ignore();
			ret2 = 
			if (ret2 != 0)
			{
				cout << "Congrats! That was the correct answer.\n\n";
				correct2 = true;
			}
			else
			{
				cout << "Nope! That was not the correct answer.\n\n";
				correct2 = false;
			}
			break;
		}
		case 2:
		{
			cout << set2q2 << endl;
			cin >> set2in;
			cin.ignore();
			if (set2in == set2ans2)
			{
				cout << "Congrats! That was the correct answer.\n\n";
				correct2 = true;
			}
			else
			{
				cout << "Nope! That was not the correct answer.\n\n";
				correct2 = false;
			}
			break;
		}
		case 3:
		{
			cout << set2q3 << endl;
			cin >> set2in;
			cin.ignore();
			if ((set2in == set2ans3pos1) || (set2in == set2ans3pos2))
			{
				cout << "Congrats! That was one of the correct answers.\n\n";
				correct2 = true;
			}
			else
			{
				cout << "Nope! That was not either of the correct answers.\n\n";
				correct2 = false;
			}
			break;
		}
		case 4:
		{
			cout << set2q4 << endl;
			cin >> set2in;
			cin.ignore();
			if (set2in == set2ans4)
			{
				cout << "Congrats! That was the correct answer.\n\n";
				correct2 = true;
			}
			else
			{
				cout << "Nope! That was not the correct answer.\n\n";
				correct2 = false;
			}
			break;
		}
		case 5:
		{
			cout << set2q5 << endl;
			cin >> set2in;
			cin.ignore();
			if ((set2in == set2ans5pos1) || (set2in == set2ans5pos2))
			{
				cout << "Congrats! That was one of the correct answers.\n\n";
				correct2 = true;
			}
			else
			{
				cout << "Nope! That was not either of the correct answers.\n\n";
				correct2 = false;
			}
			break;
		}
		default:
			break;
		}
	}

	//End
	{
		if ((correct1 && correct2 == true))
		{
			cout << "Wow! You got both of them correct! Good job!\n";
		}
		cout << "Well, that's all. Have a nice day!\n";
	}
	system("pause");
	return 0;
}
/*
set2q1 = "Who is the creator of the Mario series? (First Last)";
set2ans1 = "Shigeru Miyamoto";
set2q2 = "Who is the creator of the Kirby series? (First Last)";
set2ans2 = "Masahiro Sakuri";
set2q3 = "Who is one of the creators of the Zelda series? (First Last) (There are two possible answers)";
set2ans3pos1 = "Shigeru Miyamoto";
set2ans3pos2 = "Takashi Tezuka";
set2q4 = "Who is the current president of Nintendo? (First Last)";
set2ans4 = "Tatsumi Kimishima";
set2q5 = "Who is one of the creators of the Metroid series? (First Last) (There are two possible answers)";
set2ans5pos1 = "Gunpei Yokoi";
set2ans5pos2 = "Yoshio Sakamoto";
*/