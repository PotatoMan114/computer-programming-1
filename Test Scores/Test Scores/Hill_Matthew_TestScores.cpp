#include <iostream>
#include <vector>
#include <math.h>
#include <numeric> //accumulate function
using namespace std;

int main()
{
	vector<int> scores;
	
	//Input scores
	cout << "Please enter the test scores one by one. Enter '-1' when you are done.\n";
	for (int i = 0; ; i++)
	{
		int n;
		cin >> n;
		cin.ignore();
		if (n == -1)
			break;
		else
		{
			scores.push_back(n);
		}
	}
	system("cls");

	//Display number of perfect scores
	int perfects = 0;
	for (int i = 0; i < scores.size(); i++)
	{
		if (scores[i] == 100)
			perfects++;
	}
	cout << "There are " << perfects << " perfect scores.\n";

	//Display average
	int sum = accumulate(scores.begin(), scores.end(), 0);
	int average = sum / scores.size();
	cout << "The average score is " << average << ".\n";

	//Display lowest value
	int n = scores.size();
	int min = scores[0];
	for (int i = 1; i < n; i++)
	{
		if (scores[i] < min)
			min = scores[i];
	}
	cout << "The lowest score is " << min << ".\n";

	system("pause");
	return 0;
}