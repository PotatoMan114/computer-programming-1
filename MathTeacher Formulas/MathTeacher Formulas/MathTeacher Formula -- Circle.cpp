#include <iostream>
#include <math.h>

using namespace std;

int main()
{
	float squareSideLength;
	float squareAnswer;
	cout << "Enter the side length of the square that you wish to calculate the area of." << endl;
	cin >> squareSideLength;
	squareAnswer = squareSideLength * squareSideLength;
	cout << squareAnswer << endl;
	system("pause");
	return 0;
}