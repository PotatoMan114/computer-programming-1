#include <iostream>
#include <ctime>
#include <math.h>
#include <vector>
#include <string>
#include <iomanip>
using namespace std;
//Global variables
int die[5];
int numberTotals[6]; //Number of each number.
int playerTurn;
int scoreBoard[4][14]; //First is player, second is score slot. ***14 is yahtzee bonus!!!****
int scoreBoardBool[4][13]; //First is player, second is score slot. No need for yahtzee bonus.
bool complete;
int gameComplete;


//Declare and define custom functions

int roll() //Gives a die a value between 1 and 6 inclusive
{
	int outcome;
	outcome = rand() % (6) + 1;
	return outcome;
}

void outputDice() //Outputs all five dice
{
	cout << "Here are your dice:\n";
	cout << setw(2) << die[0] << setw(2) << die[1] << setw(2) << die[2] << setw(2) << die[3] << setw(2) << die[4] << endl;
}

void scoreboard()
{
	cout << "Here is Player " << playerTurn << "'s scoreboard:\n\n";
	if (scoreBoardBool[playerTurn][0] == 1)
		cout << "Ones:............." << scoreBoard[playerTurn][0] << "\n";
	else
		cout << "Ones:.............\n";
	if (scoreBoardBool[playerTurn][1] == 1)
		cout << "Twos:............." << scoreBoard[playerTurn][1] << "\n";
	else
		cout << "Twos:.............\n";
	if (scoreBoardBool[playerTurn][2] == 1)
		cout << "Threes:..........." << scoreBoard[playerTurn][2] << "\n";
	else
		cout << "Threes:...........\n";
	if (scoreBoardBool[playerTurn][3] == 1)
		cout << "Fours:............" << scoreBoard[playerTurn][3] << "\n";
	else
		cout << "Fours:............\n";
	if (scoreBoardBool[playerTurn][4] == 1)
		cout << "Fives:............" << scoreBoard[playerTurn][4] << "\n";
	else
		cout << "Fives:............\n";
	if (scoreBoardBool[playerTurn][5] == 1)
		cout << "Sixes:............" << scoreBoard[playerTurn][5] << "\n";
	else
		cout << "Sixes:............\n";
	if (scoreBoardBool[playerTurn][6] == 1)
		cout << "3 of a Kind:......" << scoreBoard[playerTurn][6] << "\n";
	else
		cout << "3 of a Kind:......\n";
	if (scoreBoardBool[playerTurn][7] == 1)
		cout << "4 of a Kind:......" << scoreBoard[playerTurn][7] << "\n";
	else
		cout << "4 of a Kind:......\n";
	if (scoreBoardBool[playerTurn][8] == 1)
		cout << "Full House:......." << scoreBoard[playerTurn][8] << "\n";
	else
		cout << "Full House:.......\n";
	if (scoreBoardBool[playerTurn][9] == 1)
		cout << "Small Straight:..." << scoreBoard[playerTurn][9] << "\n";
	else
		cout << "Small Straight:...\n";
	if (scoreBoardBool[playerTurn][10] == 1)
		cout << "Large Straight:..." << scoreBoard[playerTurn][10] << "\n";
	else
		cout << "Large Straight:...\n";
	if (scoreBoardBool[playerTurn][11] == 1)
		cout << "Yahtzee:.........." << scoreBoard[playerTurn][11] << "\n";
	else
		cout << "Yahtzee:..........\n";
	if (scoreBoardBool[playerTurn][12] == 1)
		cout << "Chance:..........." << scoreBoard[playerTurn][12] << "\n";
	else
		cout << "Chance:...........\n";
	system("pause");
}

void scoringMagicness()
{
	int scoreInput;
	//Puts the values of the dice into numberTotals[]
	for (int i = 0; i < 5; i++)
	{
		for (int j = 1; j <= 6; j++)
		{
			int x = j - 1;
			if (die[i] == j)
				numberTotals[x]++;
		}
	}
	//Chooses where to score
	for (;;)
	{
		outputDice();
		cout << "Where would you like to put your score?\n" <<
			//Upper Section
			"1 -- Aces" << endl <<
			"2 -- Twos" << endl <<
			"3 -- Threes" << endl <<
			"4 -- Fours" << endl <<
			"5 -- Fives" << endl <<
			"6 -- Sixes" << endl <<
			//Lower Section
			"7 -- 3 of a Kind" << endl <<
			"8 -- 4 of a Kind" << endl <<
			"9 -- Full House" << endl <<
			"10 -- Small Straight" << endl <<
			"11 -- Large Straight" << endl <<
			"12 -- Yahtzee!" << endl <<
			"13 -- Chance" << endl;
		cin >> scoreInput;
		int x = scoreInput - 1;
		if (scoreBoardBool[playerTurn][x] == 1)
		{
			cout << "You did a bad. You already put something in this slot.\nPlease try again.\n";
			system("pause");
			system("cls");
		}
		else if (scoreInput >= 1 && scoreInput <= 13)
			break;
		
		else
		{
			cout << "DO IT RIGHT, YA IDJIT\n";
			system("pause");
			system("cls");
		}
	}
	//Aces
	if (scoreInput == 1)
	{
		scoreBoardBool[playerTurn][0] = 1;
		int x = numberTotals[0];
		scoreBoard[playerTurn][0] = x;
		cout << "Your score in slot 1 is " << x << ".\n";
	}
	//Twos
	else if (scoreInput == 2)
	{
		scoreBoardBool[playerTurn][1] = 1;
		int x = (numberTotals[1] * 2);
		scoreBoard[playerTurn][1] = x;
		cout << "Your score in slot 2 is " << x << ".\n";
	}
	//Threes
	else if (scoreInput == 3)
	{
		scoreBoardBool[playerTurn][2] = 1;
		int x = (numberTotals[2] * 3);
		scoreBoard[playerTurn][2] = x;
		cout << "Your score in slot 3 is " << x << ".\n";
	}
	//Fours
	else if (scoreInput == 4)
	{
		scoreBoardBool[playerTurn][3] = 1;
		int x = (numberTotals[3] * 4);
		scoreBoard[playerTurn][3] = x;
		cout << "Your score in slot 4 is " << x << ".\n";
	}
	//Fives
	else if (scoreInput == 5)
	{
		scoreBoardBool[playerTurn][4] = 1;
		int x = (numberTotals[4] * 5);
		scoreBoard[playerTurn][4] = x;
		cout << "Your score in slot 5 is " << x << ".\n";
	}
	//Sixes
	else if (scoreInput == 6)
	{
		scoreBoardBool[playerTurn][5] = 1;
		int x = (numberTotals[5] * 6);
		scoreBoard[playerTurn][5] = x;
		cout << "Your score in slot 6 is " << x << ".\n";
	}
	//3 of a Kind
	else if (scoreInput == 7)
	{
		scoreBoardBool[playerTurn][6] = 1;
		int x;
		if ((numberTotals[0] = 3) || (numberTotals[1] = 3) || (numberTotals[2] = 3) || (numberTotals[3] = 3) || (numberTotals[4] = 3) || (numberTotals[5] = 3))
		{
			x = die[0] + die[1] + die[2] + die[3] + die[4];
		}
		else
		{
			x = 0;
		}
		scoreBoard[playerTurn][6] = x;
		cout << "Your score in slot 7 is " << x << ".\n";
	}
	//Four of a Kind
	else if (scoreInput == 8)
	{
		scoreBoardBool[playerTurn][7] = 1;
		int x;
		if ((numberTotals[0] = 4) || (numberTotals[1] = 4) || (numberTotals[2] = 4) || (numberTotals[3] = 4) || (numberTotals[4] = 4) || (numberTotals[5] = 4))
		{
			x = die[0] + die[1] + die[2] + die[3] + die[4];
		}
		else
		{
			x = 0;
		}
		scoreBoard[playerTurn][7] = x;
		cout << "Your score in slot 8 is " << x << ".\n";
	}
	//Full House
	else if (scoreInput == 9)
	{
		scoreBoardBool[playerTurn][8] = 1;
		int x;
		if (((numberTotals[0] == 3) || (numberTotals[1] == 3) || (numberTotals[2] == 3) || (numberTotals[3] == 3) || (numberTotals[4] == 3) || (numberTotals[5] == 3)) && ((numberTotals[0] == 2) || (numberTotals[1] == 2) || (numberTotals[2] == 2) || (numberTotals[3] == 2) || (numberTotals[4] == 2) || (numberTotals[5] == 2)))
		{
			x = 25;
		}
		else
		{
			x = 0;
		}
		scoreBoard[playerTurn][8] = x;
		cout << "Your score in slot 9 is " << x << ".\n";
	}
	//Small Straight
	else if (scoreInput == 10)
	{
		scoreBoardBool[playerTurn][9] = 1;
		int x;
		if (((numberTotals[0] >= 1) && (numberTotals[1] >= 1) && (numberTotals[2] >= 1) && (numberTotals[3] >= 1)) || ((numberTotals[1] >= 1) && (numberTotals[2] >= 1) && (numberTotals[3] >= 1) && (numberTotals[4] >= 1)) || ((numberTotals[2] >= 1) && (numberTotals[3] >= 1) && (numberTotals[4] >= 1) && (numberTotals[5] >= 1)))
		{
			x = 30;
		}
		else
		{
			x = 0;
		}
		scoreBoard[playerTurn][9] = x;
		cout << "Your score in slot 10 is " << x << ".\n";
	}
	//Large Straight
	else if (scoreInput == 11)
	{
		scoreBoardBool[playerTurn][10] = 1;
		int x;
		if (((numberTotals[0] == 1) && (numberTotals[1] == 1) && (numberTotals[2] == 1) && (numberTotals[3] == 1) && (numberTotals[4] == 1)) || ((numberTotals[1] == 1) && (numberTotals[2] == 1) && (numberTotals[3] == 1) && (numberTotals[4] == 1) && (numberTotals[5] == 1)))
		{
			x = 40;
		}
		else
		{
			x = 0;
		}
		scoreBoard[playerTurn][10] = x;
		cout << "Your score in slot 11 is " << x << ".\n";
	}
	//Yahtzee!
	else if (scoreInput == 12)
	{
		int x;
		if ((numberTotals[0] == 5) || (numberTotals[1] == 5) || (numberTotals[2] == 5) || (numberTotals[3] == 5) || (numberTotals[4] == 5) || (numberTotals[5] == 5))
		{
			if ((scoreBoard[playerTurn][11] == 50) && (scoreBoardBool[playerTurn][11] == 1))
			{
				scoreBoard[playerTurn][13] = scoreBoard[playerTurn][13] + 100;
				cout << "Because you already have 50 for Yahtzee, you will get 100 bonus!\n";
			}
			else if ((scoreBoard[playerTurn][11] == 0) && (scoreBoardBool[playerTurn][11] == 1))
			{
				cout << "Because you have a 0 in yahtzee, you will not get a 100 bonus.\n";
			}
			else
			{
				x = 50;
				cout << "Your score in slot 12 is " << x << ".\n";
			}
		}
		else
		{
			x = 0;
			scoreBoardBool[playerTurn][11] = x;
			cout << "Your score in slot 12 is " << x << ".\n";
		}
		scoreBoardBool[playerTurn][11] = 1;
	}
	//Chance
	else if (scoreInput == 13)
	{
		int x = die[0] + die[1] + die[2] + die[3] + die[4];
		scoreBoardBool[playerTurn][12] = 1;
		scoreBoard[playerTurn][12] = x;
		cout << "Your score in slot 13 is " << x << ".\n";
	}
	system("pause");
	system("cls");
	//Show scoreboard
	scoreboard();
	for (int i = 0; i < 6; i++)
		numberTotals[i] = 0;
}

//Main
int main()
{
	srand(time(0));
	int rollCount;
	int rollAgain;
	string player[4] = { "Player 1", "Player 2", "Player 3", "Player 4" };
	//Introduction
	int playerCount;
	cout << "Welcome!!!\nYou've signed yourself up to play Yahtzee.\nWhy would you inflict this pain upon yourself?\nThis can support up to four players!\nHow many players will play?\n";
	for (;;)
	{
		cin >> playerCount;
		cin.ignore();
		if (playerCount < 1 || playerCount > 4)
		{
			cout << "Ya done messed up, there, mate.\nTry again.\n";
		}
		else
			break;
	}
	cout << "Okay! You should know the rules, and if you don't, you can find them at https://www.hasbro.com/common/instruct/Yahtzee.pdf! \n";
	cout << "Let's begin!\n";
	system("pause");
	//Roll to see who goes first
	if (playerCount == 1)
		playerTurn = 1;
	else if (playerCount != 1)
	{
		int playerTurnRoll[4];
		for (int i = 0; i < playerCount; i++)
		{
			cout << "Here are " << player[i] << "'s dice after the roll:\n";
			for (int x = 0; x < 5; x++)
			{
				die[x] = roll();
				cout << die[x] << ", ";
			}
			cout << endl;
			playerTurnRoll[i] = die[0] + die[1] + die[2] + die[3] + die[4];
		}
		if (playerTurnRoll[0] > playerTurnRoll[1] && playerTurnRoll[0] > playerTurnRoll[2] && playerTurnRoll[0] > playerTurnRoll[3])
		{
			cout << "Player 1 will go first!\n\n";
			playerTurn = 1;
		}
		else if (playerTurnRoll[1] > playerTurnRoll[0] && playerTurnRoll[1] > playerTurnRoll[2] && playerTurnRoll[1] > playerTurnRoll[3])
		{
			cout << "Player 2 will go first!\n\n";
			playerTurn = 2;
		}
		else if (playerTurnRoll[2] > playerTurnRoll[0] && playerTurnRoll[2] > playerTurnRoll[1] && playerTurnRoll[2] > playerTurnRoll[3])
		{
			cout << "Player 3 will go first!\n\n";
			playerTurn = 3;
		}
		else if (playerTurnRoll[3] > playerTurnRoll[0] && playerTurnRoll[3] > playerTurnRoll[1] && playerTurnRoll[3] > playerTurnRoll[2])
		{
			cout << "Player 4 will go first!\n\n";
			playerTurn = 4;
		}
		system("pause");
	}
	system("cls");
	//Play the game
	do
	{
		//First roll
		rollCount = 1;
		cout << "Roll your dice, player " << playerTurn << "!\n";
		system("pause");
		for (int x = 0; x < 5; x++)
		{
			die[x] = roll();
		}
		outputDice();
		while (rollCount != 3) //Roll Again?
		{
			for (;;)
			{
				rollAgain = 0;
				cout << "Would you like to roll again?" << "\n0 - No\n1 - Yes\n";
				cin >> rollAgain;
				cin.ignore();
				if (rollAgain != 1 && rollAgain != 0)
				{
					cout << "Please enter a valid value.\n";
					system("pause");
					system("cls");
					outputDice();
				}
				else
					break;
			}
			if (rollAgain == 1) //Rolls again
			{
				rollCount++;
				system("cls");
				outputDice();
				for (int i = 0; i < 5; i++)
				{
					int boolThing[5];
					int x;
					cout << "Select a die that you would like to re-roll. (1-5, 0 when done):\n";
					cin >> x;
					
					if (x == 0)
						break;
					else if (boolThing[x] == 1)
					{
						cout << "Nice try. You already rerolled that one.\nTry again.\n";
						i--;
						system("pause");
						system("cls");
					}
					else if (x > 5 || x < 0)
					{
						cout << "DO IT RIGHT YA IDJIT!!";
						i--;
						system("pause");
					}
					else
					{
						x--;
						die[x] = roll();
					}
					boolThing[x] = 1;
				}
				outputDice();
				system("pause");
			}
			if (rollAgain == 0)
			{
				rollCount = 0;
				break;
			}
		}
		system("cls");
		scoringMagicness();
		if (playerTurn < playerCount && playerCount != 1)
		{
			playerTurn++;
			cout << "Okay, next player!\n";
			system("pause");
		}
		else
		{
			playerTurn = 1;
			cout << "Okay, next player!\n";
			system("pause");
		}
		system("cls");
		gameComplete = true;
		for (int i = 0; i < 13; i++)
		{
			for (int j = 0; j < playerCount; j++)
			{
				if (scoreBoardBool[j][i] == 0)
				{
					gameComplete = false;
				}
			}
		}
	} while (gameComplete == false); 

	//Endgame
	system("cls");
	bool bonus[4];
	int totalScore[4] = { 0,0,0,0 };
	for (int i = 0; i < 4; i++)
	{
		int x = 0;
		for (int j = 0; j < 6; j++)
		{
			x = x + scoreBoard[i][j];
		}
		if (x >= 63)
			bonus[i] = true;
	}
	for (int i = 0; i < playerCount; i++)
	{
		for (int j = 0; j < 14; j++)
		{
			totalScore[i] = totalScore[i] + scoreBoard[i][j];
		}
		if (bonus[i] == true)
		{
			totalScore[i] = totalScore[i] + 35;
		}
	}

	for (int i = 0; i < playerCount; i++)
	{
		cout << "Player " << i << "'s total score is " << totalScore[i] << ".\n";
	}

	if (playerCount != 1)
	{
		if ((totalScore[0] > totalScore[1]) && (totalScore[0] > totalScore[2]) && (totalScore[0] > totalScore[3]))
		{
			cout << "Player 1 Wins!!!!!!!!!!!!!\n";
		}
		else if ((totalScore[1] > totalScore[0]) && (totalScore[1] > totalScore[2]) && (totalScore[0] > totalScore[3]))
		{
			cout << "Player 2 Wins!!!!!!!!!!!!!\n";
		}
		else if ((totalScore[2] > totalScore[0]) && (totalScore[2] > totalScore[1]) && (totalScore[2] > totalScore[3]))
		{
			cout << "Player 3 Wins!!!!!!!!!!!!!\n";
		}
		else if ((totalScore[3] > totalScore[0]) && (totalScore[3] > totalScore[1]) && (totalScore[3] > totalScore[2]))
		{
			cout << "Player 4 Wins!!!!!!!!!!!!!\n";
		}
		else if ((totalScore[0] == totalScore[1]) || (totalScore[0] == totalScore[2]) || (totalScore[0] == totalScore[3]) || (totalScore[1] == totalScore[2]) || (totalScore[1] == totalScore[3]) || (totalScore[2] == totalScore[3]))
				cout << "It was a tie!!!!!\n";
		system("pause");
		system("cls");
	}
		cout << "I hope you had fun, if that was at all possible. Have a good day!\n";
	return 0;
}