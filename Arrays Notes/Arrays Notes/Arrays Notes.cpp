#include <iostream>
#include <string>
#include <ctime>
using namespace std;

void main()
{
	srand(time(0));
	//Declare an array
	int numbers[7];

	//Define the values
	numbers[0] = rand() % 10;
	numbers[1] = 21;
	numbers[2] = 7;
	numbers[3] = 3;
	numbers[4] = 22;
	numbers[5] = 42;
	numbers[6] = 6;
	for (int pos = 0; pos < 7; pos++)
		cout << numbers[pos] << endl;

	//Declaring and definitng
	string names[3] = { "Kathryn", "Ashley", "Daf" };
	cout << names[1] << endl;

	system("pause");
}