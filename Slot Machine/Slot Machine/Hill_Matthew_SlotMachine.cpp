#include <iostream>
#include <ctime>
#include <math.h>
#include <string>
#include <iomanip>

using namespace std;

//Custom functions declared and defined
int random() //Will generate a random number between 0 and 9 inclusive.
{
	return rand() % 9;
}
int match(int numbers[]) //Will determine the number of matches from the numbers from numbers()
{
	//Determines matches for each number.
	int matches[5] = { 0,0,0,0,0 };
	int n;
	for (n = 0; n < 5; n++)
	{
		if (numbers[n] == numbers[0])
			matches[n]++;
		if (numbers[n] == numbers[1])
			matches[n]++;
		if (numbers[n] == numbers[2])
			matches[n]++;
		if (numbers[n] == numbers[3])
			matches[n]++;
		if (numbers[n] == numbers[4])
			matches[n]++;
	}
	int match = 0;

	//Looks for 101
	if (((numbers[0] + 1) == numbers[1]) && ((numbers[1] + 1) == numbers[2]) && ((numbers[2] + 1) == numbers[3]) && ((numbers[3] + 1) == numbers[4]))
		match = 101;

	//Looks for 102
	else if (((numbers[0] - 1) == numbers[1]) && ((numbers[1] - 1) == numbers[2]) && ((numbers[2] - 1) == numbers[3]) && ((numbers[3] - 1) == numbers[4]))
		match = 102;

	//Looks for 103
	else if (((numbers[0] == numbers[2]) && (numbers[0] == numbers[4])) && (numbers[1] == numbers[3]))
		match = 103;

	//Looks for 104
	else if ((matches[0] == 2) && (matches[1] == 3) && (matches[2] == 3) && (matches[3] == 3) && (matches[4] == 2))
		match = 104;

	//Looks for 11
	else if ((matches[0] + matches[1] + matches[2] + matches[3] + matches[4]) == 13)
		match = 11;

	//Looks for 12
	else if ((matches[0] + matches[1] + matches[2] + matches[3] + matches[4]) == 9)
		match = 12;

	//Determines highest match number

	else if (matches[0] == 5)
		match = 5;
	else if ((matches[0] == 4) || (matches[1] == 4) || (matches[2] == 4) || (matches[3] == 4) || (matches[4] == 4))
		match = 4;
	else if ((matches[0] == 3) || (matches[1] == 3) || (matches[2] == 3) || (matches[3] == 3) || (matches[4] == 3))
		match = 3;
	else if ((matches[0] == 2) || (matches[1] == 2) || (matches[2] == 2) || (matches[3] == 2) || (matches[4] == 2))
		match = 2;
	else if ((matches[0] == 1) && (matches[1] == 1) && (matches[2] == 1) && (matches[3] == 1) && (matches[4] == 1))
		match = 1;
	else
	{
		cout << "Something went wrong.\n";
		system("pause");
	}
	return match;
}

//Main
int main()
{
	srand(time(0));
	//Some variables declared and some defined.
	int coins = 10;
	int gambled;
	int matches;

	bool valid;
	bool playAgain;
	string playAgainInput;
	//Introducion
	cout << "Welcome!\n";
	cout << "This program is a slot machine.\nYou will start out with 10 coins and will be asked how many you would like to gamble.\nThe program will then randomly generate five numbers.\nIt will look for any matches or patterns in the numbers.\nYou will be returned coins to you based on that and how many coins you gambled.\nIf you still have coins, you will be asked if you would like to play again.\n\n";
	system("pause");
	system("cls");
	
	//main loop.
	for(;;)
	{
		//Input coins being gambled
		cout << "Let's do this!\n";
		cout << "You currently have " << coins << " coins.\nHow many would you like to gamble?\n";
		do
		{
			cin >> gambled;
			cin.ignore();
			if (gambled > coins)
			{
				cout << "Mate, you don't have that many coins. Try again.\n";
				valid = false;
			}
			else if (gambled == 0)
			{
				cout << "You have to actually put something in if you're going to play. Try again.\n";
				valid = false;
			}
			else if (gambled < 0)
			{
				cout << "No! You don't get to just take money! You have to put something in!\n";
				valid = false;
			}
			else
			{
				coins = coins - gambled;
				valid = true;
			}
		} while (valid == false); //tests if input is valid

		system("cls");
		cout << "You're gambling " << gambled << " coins.\n";
		system("pause");

		//Generates and displays numbers given by the slot machine
		cout << "Here are the numbers:\n";
		int number[] = { random(), random(), random(), random(), random() };
		cout << setw(2) << number[0] << setw(2) << number[1] << setw(2) << number[2] << setw(2) << number[3] << setw(2) << number[4] << endl;
		system("pause");

		//Determines and saves the number of matches
		matches = match(number);

		//Changes number of coins the player has according to the number of matches.
		switch (matches)
		{
			//It may be necessary to edit the numbers to rig it more in favor of the house.
		case 1: 
			cout << "There are no matches or recognized patterns in the numbers.\nSorry, but that means you loose the coins you gambled.\n";
			coins = coins + (gambled * 0);
			break;
		case 2:
			cout << "Two of the numbers match each other, so you get back half of the coins that you gambled.\nIf necessary, the output is rounded down to the nearest whole number.\n\n";
			coins = coins + (gambled * 0.5);
			break;
		case 3:
			cout << "Three of the numbers match each other, so you get 2 times the amount of coins that you gambled returned to you.\n\n";
			coins = coins + (gambled * 2);
			break;
		case 4: 
			cout << "Four of the numbers match each other, so you get 15 times the amount of coins that you gambled returned to you.\n\n";
			coins = coins + (gambled * 15);
			break;
		case 5:
			cout << "CONGRATULATIONS!!!!\nAll five of the numbers match each other!!!\nYou get 50 times the amount of coins that you gambled returned to you!\n\n";
			coins = coins + (gambled * 50);
			break;
		case 11:
			cout << "Wow, there are two matches.\nLooks like we have one match of three and one match of two.\nThis means that you get 5 times the amount of coins that you gambled returned to you.\n\n";
			coins = coins + (gambled * 5);
			break;
		case 12:
			cout << "Wow, there are two matches.\nLooks like we have two matches of two numbers.\nThis means that you get 3 times the amount of coins that you gambled returned to you.\n\n";
			coins = coins + (gambled * 3);
			break;
		case 101:
			cout << "Looks like there is a pattern.\nThe numbers are increasing by one.\nThis means that you get 20 times the amount of coins that you gambed returned to you.\n\n";
			coins = coins + (gambled * 20);
			break;
		case 102:
			cout << "Looks like there is a pattern.\nThe numbers are decreasing by one.\nTihs means that you get 20 times the amount of coins that you gambed returned to you.\n\n";
			coins = coins + (gambled * 20);
			break;
		case 103:
			cout << "Hey, there's a pattern.\nThe numbers are alternating.\nThis means that you get 10 times the number of coins that you gambled returned to you.\n\n";
			coins = coins + (gambled * 10);
		case 104:
			cout << "Hey, there's a pattern.\nThe first and last numbers are equal to each other and the middle numbers are equal to each other.\nThis means that you get 10 times the number of coins taht you gambled returned to you.\n\n";
			coins = coins + (gambled * 10);
		default:
			cout << "Something went wrong.\n\n";
			system("pause");
		}
		cout << "With that, your current coin total is " << coins << ".\n";
		system("pause");
		system("cls");
		cout << "With that, your current coin total is " << coins << ".\n";

		//Determines if they have any coins left. If not, the game ends.
		if (coins <= 0)
		{
			cout << "Sorry, you are all out of coins. You can no longer continue playing.\n\n";
			system("pause");
			break;
		}

		//Asks if the player would like to play again.
		for(;;)
		{
			cout << "Would you like to play again with that coin total? (enter 'yes' or 'no')\n";
			getline(cin, playAgainInput);
			if (((playAgainInput[0] == 'N') || (playAgainInput[0] == 'n')) && ((playAgainInput[1] == 'O') || (playAgainInput[1] == 'o')))
			{
				playAgain = false;
				break;
			}
			else if (((playAgainInput[0] == 'Y') || (playAgainInput[0] == 'y')) && ((playAgainInput[1] == 'E') || (playAgainInput[1] == 'e')) && ((playAgainInput[2] == 'S') || (playAgainInput[2] == 's')))
			{
				playAgain = true;
				break;
			}
			else
			{
				cout << "Sorry, that inut was not recognized. Please try again.\n\n";
				system("pause");
				system("cls");
				cout << "You currently have " << coins << " coins.\n";
			}
		}
		if (playAgain == false)
		{
			break;
		}
		else if (playAgain == true)
		{
			system("cls");
			cout << "Alright! ";
		}
	} //This should be an infinite loop which will break when the user decides to leave or if they run out of coins.

	//Ending
	system("cls");
	cout << "Thanks for playing! We hope to see you again some time.\n";
	system("pause");
	return 0;
}

/*Notes
highestMatch:
	1 = no matches
	2 = two numbers match
	3 = three numbers match
	4 = four numbers match
	5 = all numbers match
	11 = three of the numbers match each other and the other two match each other, but they are different matches. i.e 1 1 1 2 2 
	12 = two of the numbers match each other, two of the other numbers match but are not the same as the other match, and one of them doesn't match any. i.e 1 1 2 2 3
	101 = Pattern: all numbers go in consecutive increasing order. i.e 0 1 2 3 4 OR 5 6 7 8 9 -- NOT 6 7 8 9 0
	102 = Pattern: all numbers go in consecutive decending order. i.e 4 3 2 1 0 OR 9 8 7 6 5 -- NOT 0 9 8 7 6
	103 = Pattern: the first number matches the third and fifth; the second number matches the fourth. i.e 3 2 3 2 3
	104 = Pattern: the first and last numbers are equal; the middle numbers are all equal. i.e. 2 1 1 1 2
*/