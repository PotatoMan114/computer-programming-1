#include <iostream>
#include <string>
#include <fstream>
using namespace std;

void main()
{
	int choice;
	string message, filename;
	char convert;
	fstream file; //Allows access to a file

	cout << "Will you encrypt or decrypt a message?" << "\n0 - Encrypt\n1 - Decrypt\n";
	cin >> choice;
	cin.ignore();

	cout << "Enter the name of the file to use:" << endl;
	getline(cin, filename);

	if (choice == 0) //Encrypting
	{
		cout << "Write your message:" << endl;
		getline(cin, message);

		file.open(filename, ios::out); //ios::out means output, overwrites existing data.
		//Aternatively ios::app - Appens to a file.

		//VERY SIMPLE ENCRYPTION
		for (int x = 0; x < message.size(); x++)
		{
			convert = message.at(x) + 26; //This works because char is a number
			//Look up an ASCII table
			file << convert; //"Writing" to the file
		}
		file.close(); //Actual writing happens
		cout << "Message encoded. Look in " << filename << "." << endl;
	}
	else //Decrypt
	{
		file.open(filename, ios::in); //Read from a file

		//Check for a file by the given name
		if (!file)
			cout << "FILE NOT FOUND!!!" << endl;

		while (file >> convert) //Checks for data sets convert equal to that character.
		{
			convert -= 26; //Decrypts
			cout << convert; //Displays character
		}

		file.close();
		cout << endl;
	}
	system("pause");
}