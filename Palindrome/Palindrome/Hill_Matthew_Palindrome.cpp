#include <iostream>
#include <string>
#include <vector>
#include <algorithm>
using namespace std;

int main()
{
	string input;
	vector<char> fixedInput;
	//Enter phrase
	cout << "Enter a word or phrase for the program to determine if it is a palindrome.\n";
	getline(cin, input);
	//Removes spaces, punctuation, and changes capitals to lower case.
	for (int i = 0; i < input.size(); i++)
	{
		if ((input[i] >= 65) && (input[i] <= 90))
		{
			int x = input[i] + 32;
			fixedInput.push_back(x);
		}
		else if ((input[i] >= 97) && (input[i] <= 122))
		{
			int x = input[i];
			fixedInput.push_back(x);
		}
	}
	//Copies reverse into vector "reverse"
	vector<char> reverse(fixedInput.size());
	reverse_copy(fixedInput.begin(), fixedInput.end(), reverse.begin());
	//Tests to see if it is a palindrome
	if (equal(fixedInput.begin(), fixedInput.end(), reverse.begin()))
		cout << "It is a palindrome.\n";
	else
	{
		cout << "It is not a palindrome.\n";
	}
	system("pause");
	return 0;
}