#include <iostream>
#include <ctime>
#include <string>
using namespace std;

int main()
{
	string word;
	char character;
	cout << "This program will, when given a word or phrase, change letters into numbers (A -> 1, B -> 2, C -> 3, etc).\n";
	cout << "Any characters other than letters within the phrase will remain the same (including spaces).\n";
	cout << "Input a word or phrase:\n";
	getline(cin, word);
	for (int pos = 0; pos < word.length(); pos++)
	{
		character = word[pos];
		switch (character)
		{
		case 'A':
		case 'a':
			cout << "1\n";
			break;
		case 'B':
		case 'b':
			cout << "2\n";
			break;
		case 'C':
		case 'c':
			cout << "3\n";
			break;
		case 'D':
		case 'd':
			cout << "4\n";
			break;
		case 'E':
		case 'e':
			cout << "5\n";
			break;
		case 'F':
		case 'f':
			cout << "6\n";
			break;
		case 'G':
		case 'g':
			cout << "7\n";
			break;
		case 'H':
		case 'h':
			cout << "8\n";
			break;
		case 'I':
		case 'i':
			cout << "9\n";
			break;
		case 'J':
		case 'j':
			cout << "10\n";
			break;
		case 'K':
		case 'k':
			cout << "11\n";
			break;
		case 'L':
		case 'l':
			cout << "12\n";
			break;
		case 'M':
		case 'm':
			cout << "13\n";
			break;
		case 'N':
		case 'n':
			cout << "14\n";
			break;
		case 'O':
		case 'o':
			cout << "15\n";
			break;
		case 'P':
		case 'p':
			cout << "16\n";
			break;
		case 'Q':
		case 'q':
			cout << "17\n";
			break;
		case 'R':
		case 'r':
			cout << "18\n";
			break;
		case 'S':
		case 's':
			cout << "19\n";
			break;
		case 'T':
		case 't':
			cout << "20\n";
			break;
		case 'U':
		case 'u':
			cout << "21\n";
			break;
		case 'V':
		case 'v':
			cout << "22\n";
			break;
		case 'W':
		case 'w':
			cout << "23\n";
			break;
		case 'X':
		case 'x':
			cout << "24\n";
			break;
		case 'Y':
		case 'y':
			cout << "25\n";
			break;
		case 'Z':
		case 'z':
			cout << "26\n";
			break;
		default:
			cout << word[pos] << endl;
		}
	}
	system("pause");
	return 0;
}