#include <iostream>
#include <math.h>
#include <vector>
#include <ctime>
#include <iomanip>
#include <string>

using namespace std;
int playerFiring = 1;
int playerNotFiring = 0;
	// ' ' = no ship present; 'B', 'O', 'Y', 'G', or 'P' = Ship present, not hit; 'X' = Ship present, hit
char ships[10][10][2];
	// ' ' = not fired at; 'O' = miss; 'X' = hit
char board[10][10][2];
int Bcount[2] = { 4,4 }, Ocount[2] = { 4,4 }, Ycount[2] = { 4,4 }, Gcount[2] = { 4,4 }, Pcount[2] = { 4,4 };

void displayShipBoard()
{
	cout << "Here is your ship board:\n\n";
	cout << "  |A|B|C|D|E|F|G|H|I|J|\n";
	for (int x = 0; x < 10; x++) //Row number
	{
		cout << x + 1;
		if (x < 9)
			cout << " ";
		for (int y = 0; y < 10; y++) //Column number
		{
			cout << "|" << ships[x][y][playerNotFiring];
		}
		cout << "|" << endl;
	}
}
void displayShipBoardTwo() //Don't ask why, this had to be done for the firing.
{
	cout << "Here is your ship board:\n\n";
	cout << "  |A|B|C|D|E|F|G|H|I|J|\n";
	for (int x = 0; x < 10; x++) //Row number
	{
		cout << x + 1;
		if (x < 9)
			cout << " ";
		for (int y = 0; y < 10; y++) //Column number
		{
			cout << "|" << ships[x][y][playerFiring];
		}
		cout << "|" << endl;
	}
}
void displayHitBoard()
{
	cout << "Here is your hit board:\n\n";
	cout << "  |A|B|C|D|E|F|G|H|I|J|\n";
	for (int x = 0; x < 10; x++) //Row number
	{
		cout << x + 1;
		if (x < 9)
			cout << " ";
		for (int y = 0; y < 10; y++) //Column number
		{
				cout << "|" << board[x][y][playerFiring];
		}
		cout << "|" << endl;
	}
}

void displayBlankBoard()
{
	cout << "  |A|B|C|D|E|F|G|H|I|J|\n";
	for (int x = 0; x < 10; x++) //Row number
	{
		cout << x + 1;
		if (x < 9)
			cout << " ";
		for (int y = 0; y < 10; y++) //Column number
		{
			cout << "|" << " ";
		}
		cout << "|" << endl;
	}
}

int fixLetter(char in)//This will convert the letter input into a useble number.
{
	switch (in)
	{
	case 'A':
		return 0;
	case 'a':
		return 0;
	case 'B':
		return 1;
	case 'b':
		return 1;
	case 'C':
		return 2;
	case 'c':
		return 2;
	case 'D':
		return 3;
	case 'd':
		return 3;
	case 'E':
		return 4;
	case 'e':
		return 4;
	case 'F':
		return 5;
	case 'f':
		return 5;
	case 'G':
		return 6;
	case 'g':
		return 6;
	case 'H':
		return 7;
	case 'h':
		return 7;
	case 'I':
		return 8;
	case 'i':
		return 8;
	case 'J':
		return 9;
	case 'j':
		return 9;
	default:
		return 10;
		break;
	}
}

//0: no winner yet; 1: Player 1 win; 2: Player 2 win
int checkWin() 
{
	int win;
	if (Bcount[0] == 0 && Ocount[0] == 0 && Ycount[0] == 0 && Gcount[0] == 0 && Pcount[0] == 0)
		win = 2;
	else if (Bcount[1] == 0 && Ocount[1] == 0 && Ycount[1] == 0 && Gcount[1] == 0 && Pcount[1] == 0)
		win = 1;
	else
		win = 0;
	return win;
}

bool validSpace(int number, int letter)
{
	bool valid;
	if (board[number][letter][playerFiring] == ' ')
	{
		valid = true;
	}
	else
	{
		valid = false;
	}
	return valid;
}

int main()
{
	char hit;
	int numberInput;
	char letterInput;
	int fixedLetterIn;
	int orientation;
	//Initializes the hit board values
	for (int x = 0; x < 10; x++)
	{
		for (int y = 0; y < 10; y++)
		{
			for (int z = 0; z < 2; z++)
			{
				board[x][y][z] = ' ';
			}
		}
	}
	//Initializes the ship board values
	for (int x = 0; x < 10; x++)
	{
		for (int y = 0; y < 10; y++)
		{
			for (int z = 0; z < 2; z++)
			{
				ships[x][y][z] = ' ';
			}
		}
	}
	
	//int blueShip[4], orangeShip[4], yellowShip[4], greenShip[4], purpleShip[4];

	//Intro
	cout << "Welcome to Battleship! TETRIS STYLE!\n";
	cout << "The only difference from normal Battleship is that the ships are tetrominoes.\nIf you don't know what that is, look it up.\n" <<
		"https://en.wikipedia.org/wiki/Tetromino \n";
	system("pause");
	system("cls");
	//Place ships
	for (int x = 0; x < 2; x++)
	{
		for (int i = 0; i < 5; i++)
		{
			system("cls");
			displayShipBoard();
			string shipType;
			if (i == 0)
				shipType = "light blue";
			else if (i == 1)
				shipType = "orange";
			else if (i == 2)
				shipType = "yellow";
			else if (i == 3)
				shipType = "green";
			else if (i == 4)
				shipType = "purple";
			bool valid;
			valid = false;
			for (;;)
			{
				cout << "Player " << x + 1 << ", please enter the TOP LEFT coordinate of where you would like to place your " << shipType << " 'ship.'\n";
				for (;;)
				{
					cout << "What is the letter of the coordinate?\n";
					cin >> letterInput;
					cin.ignore();
					fixedLetterIn = fixLetter(letterInput);
					if (fixedLetterIn > 9 || fixedLetterIn < 0)
					{
						cout << "Invalid input\n";
						system("pause");
					}
					else
						break;
				}
				for (;;)
				{
					cout << "What is the number of the coordinate?\n";
					cin >> numberInput;
					cin.ignore();
					if (numberInput <= 10 && numberInput >= 1)
					{
						numberInput--;
						break;
					}
					else
					{
						cout << "Invalid Input\n";
						system("pause");
					}
				}
				if (i != 2)
				{
					for (;;)
					{
						cout << "What orientation would you like to have it in?\n" <<
							"0 -- Normal\n" <<
							"1 -- 90 Degrees clockwise\n" <<
							"2 -- 180 Degrees clockwise\n" <<
							"3 -- 270 Degrees clockwise\n";
						cin >> orientation;
						cin.ignore();
						if (orientation < 0 || orientation > 3)
						{
							cout << "Enter a correct value!\n";
							system("pause");
						}
						else
							break;
					}
				}
				//Check to see if it does not go off the board
				if (i == 0)
				{
					if (((orientation == 1 || orientation == 3) && numberInput >= 7) || ((orientation == 0 || orientation == 2) && fixedLetterIn >= 7))
					{
						cout << "Invalid Placement!\n";
						system("pause");
					}
					else
					{
						valid = true;
						break;
					}
				}
				else if (i == 1)
				{
					if (((orientation == 0 || orientation == 2) && (numberInput >= 8 || fixedLetterIn >= 9)) || ((orientation == 1 || orientation == 3) && (numberInput >= 9 || fixedLetterIn >= 8)))
					{
						cout << "Invalid Placement\n";
						system("pause");
					}
					else if (orientation == 0)
					{
						if ((ships[numberInput][fixedLetterIn][x] != ' ') || (ships[numberInput + 1][fixedLetterIn][x] != ' ') || (ships[numberInput + 2][fixedLetterIn][x] != ' ') || (ships[numberInput + 2][fixedLetterIn + 1][x] != ' '))
						{
							cout << "Invalid Placement!\n";
							system("pause");
						}
						else
						{
							valid = true;
							break;
						}
					}
					else if (orientation == 1)
					{
						if ((ships[numberInput][fixedLetterIn][x] != ' ') || (ships[numberInput + 1][fixedLetterIn][x] != ' ') || (ships[numberInput][fixedLetterIn + 1][x] != ' ') || (ships[numberInput][fixedLetterIn + 2][x] != ' '))
						{
							cout << "Invalid Placement\n";
							system("pause");
						}
						else
						{
							valid = true;
							break;
						}
					}
					else if (orientation == 2)
					{
						if ((ships[numberInput][fixedLetterIn][x] != ' ') || (ships[numberInput][fixedLetterIn + 1][x] != ' ') || (ships[numberInput + 1][fixedLetterIn + 1][x] != ' ') || (ships[numberInput + 2][fixedLetterIn + 1][x] = ' '))
						{
							cout << "Invalid Placement\n";
							system("pause");
						}
						else
						{
							valid = true;
							break;
						}
					}
					else if (orientation == 3)
					{
						if ((ships[numberInput + 1][fixedLetterIn][x] != ' ') || (ships[numberInput + 1][fixedLetterIn + 1][x] != ' ') || (ships[numberInput + 1][fixedLetterIn + 2][x] != ' ') || (ships[numberInput][fixedLetterIn + 2][x] != ' '))
						{
							cout << "Invalid Placemnet\n";
							system("pause");
						}
						else
						{
							valid = true;
							break;
						}
					}
					else
					{
						valid = true;
						break;
					}
				}
				else if (i == 2)
				{
					if (numberInput == 9 || fixedLetterIn == 9)
					{
						cout << "Invalid Placement!\n";
						system("pause");
					}
					else if ((ships[numberInput][fixedLetterIn][x] != ' ') || (ships[numberInput + 1][fixedLetterIn][x] != ' ') || (ships[numberInput + 1][fixedLetterIn + 1][x] != ' ') || (ships[numberInput][fixedLetterIn + 1][x] != ' '))
					{
						cout << "Invalid Placement\n";
						system("pause");
					}
					else
					{
						valid = true;
						break;
					}
				}
				else if (i == 3)
				{
					if (((orientation == 0 || orientation == 2) && (numberInput >= 8 || fixedLetterIn >= 9)) || ((orientation == 1 || orientation == 3) && (numberInput >= 9 || fixedLetterIn >= 8)))
					{
						cout << "Invalid Placement!\n";
						system("pause");
					}
					else if (orientation == 0 || orientation == 2)
					{
						if ((ships[numberInput][fixedLetterIn][x] != ' ') || (ships[numberInput + 1][fixedLetterIn][x] != ' ') || (ships[numberInput + 1][fixedLetterIn + 1][x] != ' ') || (ships[numberInput + 2][fixedLetterIn + 1][x] != ' '))
						{
							cout << "Invalid Placement\n";
							system("pause");
						}
						else
						{
							valid = true;
							break;
						}
					}
					else if (orientation == 1 || orientation == 3)
					{
						if ((ships[numberInput + 1][fixedLetterIn][x] != ' ') || (ships[numberInput + 1][fixedLetterIn + 1][x] != ' ') || (ships[numberInput][fixedLetterIn + 1][x] != ' ') || (ships[numberInput][fixedLetterIn + 2][x] != ' '))
						{
							cout << "Invcalid Placement\n";
							system("pause");
						}
						else
						{
							valid = true;
							break;
						}
					}
					else
					{
						valid = true;
						break;
					}
				}
				else if (i == 4)
				{
					if (((orientation == 0 || orientation == 2) && (numberInput >= 9 || fixedLetterIn >= 8)) || ((orientation == 1 || orientation == 3) && (fixedLetterIn >= 9 || numberInput >= 8)))
					{
						cout << "Invalid Placement!\n";
						system("pause");
					}
					else if (orientation == 0)
					{
						if ((ships[numberInput][fixedLetterIn][x] != ' ') || (ships[numberInput][fixedLetterIn + 1][x] != ' ') || (ships[numberInput + 1][fixedLetterIn + 1][x] != ' ') || (ships[numberInput][fixedLetterIn + 2][x] != ' '))
						{
							cout << "Invalid Placement\n";
							system("pause");
						}
						else
						{
							valid = true;
							break;
						}
					}
					else if (orientation == 1)
					{
						if ((ships[numberInput][fixedLetterIn + 1][x] != ' ') || (ships[numberInput + 1][fixedLetterIn][x] != ' ') || (ships[numberInput + 1][fixedLetterIn + 1][x] != ' ') || (ships[numberInput + 2][fixedLetterIn + 1][x] != ' '))
						{
							cout << "Invalid Placement\n";
							system("pause");
						}
						else
						{
							valid = true;
							break;
						}
					}
					else if (orientation == 2)
					{
						if ((ships[numberInput + 1][fixedLetterIn][x] != ' ') || (ships[numberInput + 1][fixedLetterIn + 1][x] != ' ') || (ships[numberInput][fixedLetterIn + 1][x] != ' ') || (ships[numberInput + 1][fixedLetterIn + 2][x] != ' '))
						{
							cout << "Invalid Placement\n";
							system("pause");
						}
						else
						{
							valid = true;
							break;
						}
					}
					else if (orientation == 3)
					{
						if ((ships[numberInput][fixedLetterIn][x] != ' ') || (ships[numberInput + 1][fixedLetterIn][x] != ' ') || (ships[numberInput + 1][fixedLetterIn + 1][x] != ' ') || (ships[numberInput + 2][fixedLetterIn][x] != ' '))
						{
							cout << "Invalid Placement\n";
							system("pause");
						}
						else
						{
							valid = true;
							break;
						}
					}
					else
					{
						valid = true;
						break;
					}
				}
			}
			//Places ship
			if (valid == true)
			{
				if (i == 0)
				{
					if (orientation == 1 || orientation == 3)
					{
						ships[numberInput][fixedLetterIn][x] = 'B';
						ships[numberInput + 1][fixedLetterIn][x] = 'B';
						ships[numberInput + 2][fixedLetterIn][x] = 'B';
						ships[numberInput + 3][fixedLetterIn][x] = 'B';
					}
					else if (orientation == 0 || orientation == 2)
					{
						ships[numberInput][fixedLetterIn][x] = 'B';
						ships[numberInput][fixedLetterIn + 1][x] = 'B';
						ships[numberInput][fixedLetterIn + 2][x] = 'B';
						ships[numberInput][fixedLetterIn + 3][x] = 'B';
					}
					displayShipBoard();
					system("pause");
				}
				else if (i == 1)
				{
					if (orientation == 0)
					{
						ships[numberInput][fixedLetterIn][x] = 'O';
						ships[numberInput + 1][fixedLetterIn][x] = 'O';
						ships[numberInput + 2][fixedLetterIn][x] = 'O';
						ships[numberInput + 2][fixedLetterIn + 1][x] = 'O';
					}
					else if (orientation == 1)
					{
						ships[numberInput][fixedLetterIn][x] = 'O';
						ships[numberInput + 1][fixedLetterIn][x] = 'O';
						ships[numberInput][fixedLetterIn + 1][x] = 'O';
						ships[numberInput][fixedLetterIn + 2][x] = 'O';
					}
					else if (orientation == 2)
					{
						ships[numberInput][fixedLetterIn][x] = 'O';
						ships[numberInput][fixedLetterIn + 1][x] = 'O';
						ships[numberInput + 1][fixedLetterIn + 1][x] = 'O';
						ships[numberInput + 2][fixedLetterIn + 1][x] = 'O';
					}
					else if (orientation == 3)
					{
						ships[numberInput + 1][fixedLetterIn][x] = 'O';
						ships[numberInput + 1][fixedLetterIn + 1][x] = 'O';
						ships[numberInput + 1][fixedLetterIn + 2][x] = 'O';
						ships[numberInput][fixedLetterIn + 2][x] = 'O';
					}
					displayShipBoard();
					system("pause");
				}
				else if (i == 2)
				{
					ships[numberInput][fixedLetterIn][x] = 'Y';
					ships[numberInput + 1][fixedLetterIn][x] = 'Y';
					ships[numberInput + 1][fixedLetterIn + 1][x] = 'Y';
					ships[numberInput][fixedLetterIn + 1][x] = 'Y';
					displayShipBoard();
					system("pause");
				}
				else if (i == 3)
				{
					if (orientation == 0 || orientation == 2)
					{
						ships[numberInput][fixedLetterIn][x] = 'G';
						ships[numberInput + 1][fixedLetterIn][x] = 'G';
						ships[numberInput + 1][fixedLetterIn + 1][x] = 'G';
						ships[numberInput + 2][fixedLetterIn + 1][x] = 'G';
					}
					else if (orientation == 1 || orientation == 3)
					{
						ships[numberInput + 1][fixedLetterIn][x] = 'G';
						ships[numberInput + 1][fixedLetterIn + 1][x] = 'G';
						ships[numberInput][fixedLetterIn + 1][x] = 'G';
						ships[numberInput][fixedLetterIn + 2][x] = 'G';
					}
					displayShipBoard();
					system("pause");
				}
				else if (i == 4)
				{
					if (orientation == 0)
					{
						ships[numberInput][fixedLetterIn][x] = 'P';
						ships[numberInput][fixedLetterIn + 1][x] = 'P';
						ships[numberInput + 1][fixedLetterIn + 1][x] = 'P';
						ships[numberInput][fixedLetterIn + 2][x] = 'P';
					}
					else if (orientation == 1)
					{
						ships[numberInput][fixedLetterIn + 1][x] = 'P';
						ships[numberInput + 1][fixedLetterIn][x] = 'P';
						ships[numberInput + 1][fixedLetterIn + 1][x] = 'P';
						ships[numberInput + 2][fixedLetterIn + 1][x] = 'P';
					}
					else if (orientation == 2)
					{
						ships[numberInput + 1][fixedLetterIn][x] = 'P';
						ships[numberInput + 1][fixedLetterIn + 1][x] = 'P';
						ships[numberInput][fixedLetterIn + 1][x] = 'P';
						ships[numberInput + 1][fixedLetterIn + 2][x] = 'P';
					}
					else if (orientation == 3)
					{
						ships[numberInput][fixedLetterIn][x] = 'P';
						ships[numberInput + 1][fixedLetterIn][x] = 'P';
						ships[numberInput + 1][fixedLetterIn + 1][x] = 'P';
						ships[numberInput + 2][fixedLetterIn][x] = 'P';
					}
					displayShipBoard();
					system("pause");
				}
			}
			else
				i--;
			if (i == 4)
			{
				if (x != 1)
				{
					cout << "Next player!\n";
				}
				playerNotFiring++;
				playerFiring--;
			}
		}
	}
	system("cls");
	cout << "Okay, Fight!!\n";
	system("pause");
	system("cls");
	playerFiring = 0;
	playerNotFiring = 1;
	bool BSunkPrior = false, OSunkPrior = false, YSunkPrior = false, GSunkPrior = false, PSunkPrior = false;
	//FIGHT!!
	for (;;)
	{
		int numShot, fixedLetShot;
		char letterShot;
		bool validShot = false;
		cout << "Player " << playerFiring + 1 << " is firing!\n";
		system("pause");
		system("cls");
		displayShipBoardTwo();
		displayHitBoard();
		for (;;)
		{
			for (;;)
			{
				cout << "What is the letter of the coordinate you would like to shoot?\n";
				cin >> letterShot;
				cin.ignore();
				fixedLetShot = fixLetter(letterShot);
				if (fixedLetShot > 9 || fixedLetShot < 0)
				{
					cout << "Invalid input\n";
					system("pause");
				}
				else
					break;
			}
			for (;;)
			{
				cout << "What is the number of the coordinate you would like to shoot?\n";
				cin >> numShot;
				cin.ignore();
				if (numShot <= 10 && numShot >= 1)
				{
					numShot--;
					break;
				}
				else
				{
					cout << "Invalid Input\n";
					system("pause");
				}
			}
			validShot = validSpace(numShot, fixedLetShot);
			if (validShot == true)
				break;
			else
			{
				cout << "You have already shot there!\n";
				system("pause");
			}
		}
		//Checks what was hit
		hit = ships[numShot][fixedLetShot][playerNotFiring];
		//Says what was hit
		if (hit == 'B')
		{
			cout << "You hit the blue ship!\n";
			board[numShot][fixedLetShot][playerFiring] = 'X';
			ships[numShot][fixedLetShot][playerNotFiring] = 'X';
			Bcount[playerNotFiring]--;
		}
		else if (hit == 'O')
		{
			cout << "You hit the orange ship!\n";
			board[numShot][fixedLetShot][playerFiring] = 'X';
			ships[numShot][fixedLetShot][playerNotFiring] = 'X';
			Ocount[playerNotFiring]--;
		}
		else if (hit == 'Y')
		{
			cout << "You hit the yellow ship!\n";
			board[numShot][fixedLetShot][playerFiring] = 'X';
			ships[numShot][fixedLetShot][playerNotFiring] = 'X';
			Ycount[playerNotFiring]--;
		}
		else if (hit == 'G')
		{
			cout << "You hit the green ship!\n";
			board[numShot][fixedLetShot][playerFiring] = 'X';
			ships[numShot][fixedLetShot][playerNotFiring] = 'X';
			Gcount[playerNotFiring]--;
		}
		else if (hit == 'P')
		{
			cout << "You hit the purple ship!\n";
			board[numShot][fixedLetShot][playerFiring] = 'X';
			ships[numShot][fixedLetShot][playerNotFiring] = 'X';
			Pcount[playerNotFiring]--;
		}
		else
		{
			cout << "Miss!\n";
			board[numShot][fixedLetShot][playerFiring] = 'M';
			ships[numShot][fixedLetShot][playerNotFiring] = 'M';
		}
		//Says if something was sunk and what it was
		if ((Bcount[playerNotFiring] == 0) && (!BSunkPrior))
		{
			cout << "You sunk the blue ship!\n";
			BSunkPrior = true;
		}
		if ((Ocount[playerNotFiring] == 0) && (!OSunkPrior))
		{
			cout << "You sunk the orange ship!\n";
			OSunkPrior = true;
		}
		if ((Ycount[playerNotFiring] == 0) && (!YSunkPrior))
		{
			cout << "You sunk the yellow ship!\n";
			YSunkPrior = true;
		}
		if ((Gcount[playerNotFiring] == 0) && (!GSunkPrior))
		{
			cout << "You sunk the green ship!\n";
			GSunkPrior = true;
		}
		if ((Pcount[playerNotFiring] == 0) && (!PSunkPrior))
		{
			cout << "You sunk the purple ship!\n";
			PSunkPrior = true;
		}
		system("pause");
		system("cls");
		if (checkWin() == 0)
		{
			if (playerFiring == 0)
				playerFiring++;
			else
				playerFiring--;
			if (playerNotFiring == 0)
				playerNotFiring++;
			else
				playerNotFiring--;
		}
		else
			break;
	}
	cout << "Player " << checkWin() << " is the winner!\nNow go home.\n";
	system("pause");
	system("cls");
	return 0;
}	